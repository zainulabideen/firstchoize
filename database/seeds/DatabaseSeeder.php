<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user = User::create(array(
      'name'    => 'Admin',
      'mobile_number'         => '906611390',
      'password'      => bcrypt('12345'),
      'is_admin'         => 1,
      'status'         => 'Y',
      'activated'     => true
  ));
    }
}
