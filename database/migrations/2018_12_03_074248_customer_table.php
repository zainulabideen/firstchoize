<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerTable extends Migration
{
  
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            
            $table->increments('customer_id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->bigInteger('mobile_number')->nullable();
            $table->string('password')->nullable();
            $table->string('email_id')->nullable();
            $table->boolean('is_otp_verified')->nullable();
            $table->string('city')->nullable();
            $table->string('country_name')->nullable();
            $table->string('country_code')->nullable();
            $table->string('device_type')->nullable();
            $table->string('device_token')->nullable();
            $table->string('gender')->nullable();
            $table->string('customer_status')->nullable();
            $table->string('customer_image')->nullable();
            $table->longtext('customer_lat')->nullable();
            $table->longtext('customer_long')->nullable();
            $table->double('app_version_ios')->nullable();
            $table->double('app_version_android')->nullable();
            $table->string('address')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
              $table->timestamps();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
    
}
