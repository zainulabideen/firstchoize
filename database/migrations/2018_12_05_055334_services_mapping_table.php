<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServicesMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('services_mapping', function (Blueprint $table) {
            
            $table->increments('mapping_id');
            $table->integer('service_id')->nullable()->unsigned();
            $table->integer('sub_service_id')->nullable()->unsigned();
            $table->integer('sub_service_level1_id')->nullable()->unsigned();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->foreign('service_id')->references('service_id')->on('services')->onDelete('cascade');
            $table->foreign('sub_service_id')->references('sub_service_id')->on('sub_services')->onDelete('cascade');
                $table->foreign('sub_service_level1_id')->references('sub_service_level1_id')->on('sub_services_level1')->onDelete('cascade');
            $table->timestamps();
            
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
