<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->bigInteger('mobile_number')->unique();
          $table->string('password');
          $table->integer('service_provider_id')->nullable();
          $table->integer('customer_id')->nullable();
          $table->integer('is_admin')->default(0);
          $table->integer('is_service_provider')->default(0);
          $table->integer('is_customer')->default(0);
          $table->dateTime('registered_date')->nullable();
          $table->dateTime('updated_date')->nullable();
          $table->boolean('activated')->default(false);
          $table->string('status')->default('N');
          $table->dateTime('last_status_changed')->nullable();
          $table->macAddress('device')->nullable();
          $table->string('client_id')->nullable();
          $table->string('token')->nullable();
          $table->ipAddress('registered_ip')->nullable();
          $table->rememberToken();
          $table->string('updated_by')->nullable();
          $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
