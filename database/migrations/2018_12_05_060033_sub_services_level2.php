<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubServicesLevel2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('sub_services_level2', function (Blueprint $table) {
            
            $table->increments('sub_service_level2_id');
            $table->integer('sub_service_level1_id')->nullable()->unsigned();
            $table->string('sub_service_level2_name')->nullable();
            $table->string('sub_service_level2_type')->nullable();
            $table->string('sub_service_level2_description')->nullable();
            $table->string('sub_service_level2_keywords')->nullable();
            $table->string('sub_service_level2_category')->nullable();
            $table->string('sub_service_level2_status')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->foreign('sub_service_level1_id')->references('sub_service_level1_id')->on('sub_services_level1')->onDelete('cascade');
            $table->timestamps();
            
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
