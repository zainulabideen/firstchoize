<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubServicesLevel1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('sub_services_level1', function (Blueprint $table) {
            
            $table->increments('sub_service_level1_id');
            $table->string('sub_service_level1_name')->nullable();
            $table->string('sub_service_level1_type')->nullable();
            $table->string('sub_service_level1_description')->nullable();
            $table->string('sub_service_level1_keywords')->nullable();
            $table->string('sub_service_level1_category')->nullable();
            $table->string('sub_service_level1_status')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
            
          });
    }

    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
