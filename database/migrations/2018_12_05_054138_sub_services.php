<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('sub_services', function (Blueprint $table) {
            
            $table->increments('sub_service_id');
            $table->integer('service_id')->nullable()->unsigned();
            $table->string('sub_service_name')->nullable();
            $table->string('sub_service_type')->nullable();
            $table->string('sub_service_description')->nullable();
            $table->string('sub_service_keywords')->nullable();
            $table->string('sub_service_category')->nullable();
            $table->string('sub_service_status')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->foreign('service_id')->references('service_id')->on('services')->onDelete('cascade');
            $table->timestamps();
            
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
