<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServiceFormCreation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_form', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('service_title')->nullable();
            $table->string('service_type')->nullable();
            $table->string('service_description')->nullable();
            $table->string('service_keywords')->nullable();
            $table->string('service_category')->nullable();
            $table->string('service_parent')->nullable();
            $table->string('address')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
            
          });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
