// Shift Creation
$(function() {

    $("#shift_creation").validate({

        rules: {
            shift_name: "required",
            shift_time_from: "required",
            shift_time_to: "required"
        },
        messages: {
            shift_name: "Please Enter Shift Name",
            shift_time_from: "Please Enter Shift Time From",
            shift_time_to: "Please Enter Shift Time To"
        },

        submitHandler: function(form) {
            form.submit();
        }
    });

  });

// Route Management
$(function() {

    $("#route_management").validate({
        rules: {
            route_area: "required"
        },
        messages: {
            route_area: "Please Enter Route Area"
        },

        submitHandler: function(form) {
            form.submit();
        }
    });

  });

// Employee Register
$(function() {

    $("#employee_register").validate({

        rules: {
            employee_code: "required",
            employee_name: "required",
            mobile_number: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
              },
            email_id: {
                required: true,
                email: true
            },
            gender: "required",
            department: "required",
            pickup_time: "required",
            drop_time: "required",
            // employee_photo: "required",
            blood_group: "required",
            route: "required",
            employee_shift: "required",
            pickup_address: "required",
            company_branch: "required"
        },
        messages: {
            employee_code: "Please Enter Employee Code",
            employee_name: "Please Enter Employee Name",
            mobile_number:{
               required : "Please Enter Mobile Number"
            },
            email_id: {
                required: "Please Enter Email ID"
            },
            gender: "Please Select Gender",
            department: "Please Select Department",
            pickup_time: "Please Enter Pickup Time",
            drop_time: "Please Enter Drop Time",
            // employee_photo: "Please Select Employee Photo",
            blood_group: "Please Select Blood Group",
            route: "Please Select Route",
            employee_shift: "Please Select Employee Shift",
            pickup_address: "Please Enter Pickup Address",
            company_branch: "Please Select Company Branch"
        },

        submitHandler: function(form) {
            form.submit();
        }
    });

  });

// Driver Register
$(function() {

    $("#driver_register").validate({

        rules: {
            driver_name: "required",
            mobile_number: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
              },
            email_id: {
                required: true,
                email: true
            },
            address: "required",
            // driver_photo: "required",
            blood_group: "required",
            emr_contact_number1: "required",
            emr_contact_number2: "required",
            emr_contact_address: "required",
            dl_number: "required",
            dl_validity: "required",
            // dl_photo: "required"
        },
        messages: {
            driver_name: "Please Enter Driver Name",
            mobile_number:{
               required : "Please Enter Mobile Number"
            },
            email_id: {
                required: "Please Enter Email ID"
            },
            address: "Please Enter Address",
            // driver_photo: "Please Select Driver Photo",
            blood_group: "Please Enter Blood Group",
            emr_contact_number1: "Please Enter Emergency Contact Number 1",
            emr_contact_number2: "Please Enter Emergency Contact Number 2",
            emr_contact_address: "Please Enter Emergency Contact Address",
            dl_number: "Please Enter DL Number",
            dl_validity: "Please Enter DL Validity",
            // dl_photo: "Please Enter DL Photo"
        },

        submitHandler: function(form) {
            form.submit();
        }
    });

  });

// Vehicle Register
$(function() {

    $("#vehicle_register").validate({

        rules: {
            owner_name: "required",
            mobile_number: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
              },
            email_id: {
                required: true,
                email: true
            },
            address: "required",
            vehicle_type: "required",
            vehicle_number: "required",
            vehicle_model: "required",
            manufacturing_year: "required",
            total_seats: "required",
            tax_number: "required",
            tax_validity: "required",
            // tax_photo: "required",
            rc_number: "required",
            rc_validity: "required",
            // rc_photo: "required",
            permit_number: "required",
            permit_validity: "required",
            // permit_photo: "required",
            insurance_number: "required",
            insurance_validity: "required",
            // insurance_photo: "required",
            fc_number: "required",
            fc_validity: "required",
            fc_photo: "required"
        },
        messages: {
            driver_name: "Please Enter Driver Name",
            mobile_number:{
               required : "Please Enter Mobile Number"
            },
            email_id: {
                required: "Please Enter Email ID"
            },
            address: "Please Enter Address",
            vehicle_type: "Please Enter Vehicle Type",
            vehicle_number: "Please Enter Vehicle Number",
            vehicle_model: "Please Enter Vehicle Model",
            manufacturing_year: "Please Enter Manufacturing Year",
            total_seats: "Please Enter Total Seats",
            tax_number: "Please Enter Tax Number",
            tax_validity: "Please Enter Tax Validity",
            // tax_photo: "Please Enter Tax Photo",
            rc_number : "Please Enter RC Number",
            rc_validity : "Please Enter RC Validity",
            // rc_photo: "Please Select RC Photo",
            permit_number: "Please Enter Permit Number",
            permit_validity: "Please Enter Permit Validity",
            // permit_photo: "Please Select Permit Photo",
            insurance_number: "Please Enter Insurance Number",
            insurance_validity: "Please Enter Insurance Validity",
            insurance_photo: "Please Enter Insurance Photo",
            fc_number: "Please Enter FC Number",
            fc_validity: "Please Enter FC Validity",
            // fc_photo: "Please Select FC Photo",
        },

        submitHandler: function(form) {
            form.submit();
        }
    });

  });

// Escort Register
$(function() {

    $("#escort_register").validate({

        rules: {
            escort_name: "required",
            mobile_number: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
              },
            blood_group: "required",
            branch: "required"
        },
        messages: {
            escort_name: "Please Enter Escort Name",
            mobile_number: {
               required : "Please Enter Mobile Number"
            },
            blood_group: "Please Enter Blood Group",
            branch: "Please Enter Branch"
        },

        submitHandler: function(form) {
            form.submit();
        }
    });

  });

// Driver Vehicle Merge
$(function() {

    $("#driver_vehicle_merge").validate({

        rules: {
            vehicle_number: "required",
            driver_name: "required",
            route_name: "required"
        },
        messages: {
            vehicle_number: "Please Select Vehicle Number",
            driver_name: "Please Select Driver Name",
            route_name: "Please Select Route Name"
        },

        submitHandler: function(form) {
            form.submit();
        }
    });

  });

// Swap Vehicle
$(function() {

    $("#swap_vehicle").validate({

        rules: {
            existing_vehicle_number: "required",
            replace_vehicle_number: "required"
        },
        messages: {
            existing_vehicle_number: "Please Select Existing Vehicle Number",
            replace_vehicle_number: "Please Select Replace Vehicle Number"
        },

        submitHandler: function(form) {
            form.submit();
        }
    });

  });

// Extra Cab Request
$(function() {

    $("#extra_cab_request").validate({

        rules: {
            employee_details: "required",
            reason: "required",
            date: "required",
            time: "required",
            drop_location: "required"
        },
        messages: {
            employee_details: "Please Select Employee Details",
            reason: "Please Enter Reason",
            date: "Please Select Date",
            time: "Please Select Time",
            drop_location: "Please Enter Drop Location"
        },

        submitHandler: function(form) {
            form.submit();
        }
    });

  });

// Adhoc Request
$(function() {

    $("#adhoc_request").validate({

        rules: {
            company_branch: "required",
            booked_by: "required",
            guest_name: "required",
            guest_cell_number:  {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
              },
            report_at: "required",
            trip_to: "required",
            date: "required",
            time: "required",
            driver_name: "required",
            driver_cell_number:  {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
              },
            car_number: "required",
            closed_by: "required",
            other_city: "required",
            closing_kms: "required",
            closing_date: "required",
            closing_time: "required",
            opening_kms: "required",
            opening_date: "required",
            opening_time: "required",
            total_kms: "required",
            toll_parking: "required",
            from_office: "required",
            panctuality: "required",
            courtesy: "required",
            condition: "required",
            reson_for_complaints: "required"

        },
        messages: {
            company_branch: "Please Select Company Branch",
            booked_by: "Please Select Booked By",
            guest_name: "Please Enter Guest Name",
            guest_cell_number:  {
               required : "Please Enter Mobile Number"
            },
            report_at: "Please Enter Report At",
            trip_to: "Please Enter Tript To",
            date: "Please Select Date",
            time: "Please Enter Time",
            driver_name: "Please Enter Driver Name",
            driver_cell_number:  {
               required : "Please Enter Mobile Number"
            },
            car_number: "Please Enter Car Number",
            closed_by: "Please Enter Closed By",
            other_city: "Please Select Other City",
            closing_kms: "Please Enter Closing Kms",
            closing_date: "Please Select Closing Date",
            closing_time: "Please Enter Closing Time",
            opening_kms: "Please Enter Opening Kms",
            opening_date: "Please Select Opening Date",
            opening_time: "Please Enter Opening Time",
            total_kms: "Please Enter Total Kms",
            toll_parking: "Please Enter Toll Parking",
            from_office: "Please Enter From Office",
            panctuality: "Please Select Panctuality",
            courtesy: "Please Select Courtesy",
            condition: "Please Enter Condition",
            reson_for_complaints: "Please Enter Reason For Complaints"
        },

        submitHandler: function(form) {
            form.submit();
        }
    });

  });

// Allocate Vehicle
$(function() {

    $("#allocate_vehicle").validate({

        rules: {
            vehicle_id: "required",
            employee_id: "required"
        },
        messages: {
            vehicle_number: "Please Select Vehicle Number",
            employee_id: "Please Select employee"
        },

        submitHandler: function(form) {
            form.submit();
        }
    });

  });

// Allocate Extra Cab
$(function() {

    $("#allocate_extra_cab").validate({

        rules: {
            vehicle_number: "required"
        },
        messages: {
            vehicle_number: "Please Select Vehicle Number"
        },

        submitHandler: function(form) {
            form.submit();
        }
    });

  });
