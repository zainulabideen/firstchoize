

/* Login Form Starts*/
$('.form').find('input, textarea').on('keyup blur focus', function(e) {

    var $this = $(this),
        label = $this.prev('label');

    if (e.type === 'keyup') {
        if ($this.val() === '') {
            label.removeClass('active highlight');
        } else {
            label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
        if ($this.val() === '') {
            label.removeClass('active highlight');
        } else {
            label.removeClass('highlight');
        }
    } else if (e.type === 'focus') {

        if ($this.val() === '') {
            label.removeClass('highlight');
        } else if ($this.val() !== '') {
            label.addClass('highlight');
        }
    }

});

$('.tab a').on('click', function(e) {

    e.preventDefault();

    $(this).parent().addClass('active');
    $(this).parent().siblings().removeClass('active');

    target = $(this).attr('href');

    $('.tab-content > div').not(target).hide();

    $(target).fadeIn(600);

});
/* Login Form Ends*/

/*Open Search Bar*/
$(document).ready(function() {
    $("#search_bar").click(function() {
        $(".search-bar").slideDown("300");
        $(".search-bar").animate({ top: 0 }, 300);
    });
});

/* Close Search bar*/

$(document).ready(function(c) {
    $('.alert-close').on('click', function(c) {
        $(".search-bar").animate({ top: -50 }, 300);
        $(".search-bar").slideUp("300");
    });
});

$(document).on('click', function (e) {
    if ($(e.target).closest(".search-bar").length === 0) {
        $(".search-bar").animate({ top: -50 }, 300);
        $(".search-bar").slideUp("300")
    }
});




/*Expand and Compress Screen*/

$(function() {
    // check native support
    //$('#support').text($.fullscreen.isNativelySupported() ? 'supports' : 'doesn\'t support');

    // open in fullscreen
    $('#fullscreen .requestfullscreen').click(function() {
        $('#fullscreen').fullscreen();
        return false;
    });

    // exit fullscreen
    $('#fullscreen .exitfullscreen').click(function() {
        $.fullscreen.exit();
        return false;
    });

    // document's event
    $(document).bind('fscreenchange', function(e, state, elem) {
        // if we currently in fullscreen mode
        if ($.fullscreen.isFullScreen()) {
            $('#fullscreen .requestfullscreen').hide();
            $('#fullscreen .exitfullscreen').show();
        } else {
            $('#fullscreen .requestfullscreen').show();
            $('#fullscreen .exitfullscreen').hide();
        }

        $('#state').text($.fullscreen.isFullScreen() ? '' : 'not');
    });
});

/* Type Ahead*/

function ModalPass(modelid) {    
    $(".select2").select2({
        dropdownParent: $("#" + modelid)
    });        
};
