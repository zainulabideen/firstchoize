<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use JWTAuth;
use DB;
use Auth;
use Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{

                $result = DB::select('select * from services');

                if($request->mobile_user == 1){

                    return response()->json(
                            ['status'=>true,
                            'status_code' => 200,
                            'services' =>  $result
                            ]
                            ,200);
                    
                }else{
                    $data["results"] = $result;

                     return view('add_service',$data);
                }

        }catch(\Exception $e){
            return response()->json(
                ['status'=>true,
                'error'=>$e->getMessage()]
                ,400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info($request->all());

        try{

               $service_id=DB::table('services')->insertGetId([
                 'service_name'=>$request->service_name,
                 'service_type'=>$request->service_type,
                 'service_description'=>$request->service_description,
                 'service_keywords'=>$request->service_keywords,
                 'service_status'=>$request->service_status,
                 'service_category'=>$request->service_category,
                 'created_at'=>Carbon::now()
               ]);



            return back()->with('message', 'service inserted successfully');



         }catch(\Exception $e){
            return response()->json(['status'=>true,
            'error'=>$e->getMessage()],400);
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {

        try{
             $result = DB::select('select * from services where service_id = '.$id);

              if($request->mobile_user == 1){

                    return response()->json(
                            ['status'=>true,
                            'status_code' => 200,
                            'services' =>  $result
                            ]
                            ,200);

              }else{
                return back()->with('message', 'service fetch successfully');
              }

            

        }catch(\Exception $e){
            return response()->json(
                ['status'=>true,
                'error'=>$e->getMessage()]
                ,400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            $service_detail= DB::select('select * from services where service_id ='.$id);

            if(empty($request->service_name)) {$service_name=$service_detail[0]->service_name;  } 
            else {$service_name=$request->service_name;}
            if(empty($request->service_type)) {$service_type=$service_detail[0]->service_type;  } 
            else {$service_type=$request->service_type;}
            if(empty($request->service_description)) {$service_description=$service_detail[0]->service_description;  }
             else {$service_description=$request->service_description;}
            if(empty($request->service_keywords)) {$service_keywords=$service_detail[0]->service_keywords;  } else {$service_keywords=$request->service_keywords;}
            if(empty($request->service_status)) {$service_status=$service_detail[0]->service_status;  } else {$service_status=$request->service_status;}
            if(empty($request->service_category)) {$service_category=$service_detail[0]->service_category;  } else {$service_category=$request->service_category;}


             DB::table('services')->where('service_id',$id)->update([
             'service_name'=>$request->service_name,
             'service_type'=>$request->service_type,
             'service_description'=>$request->service_description,
             'service_keywords'=>$request->service_keywords,
             'service_status'=>$request->service_status,
             'service_category'=>$request->service_category,
             'updated_at' => Carbon::now()
            ]);


            return back()->with('message', 'service updated successfully');

        }catch(\Exception $e){
            return response()->json(
                ['status'=>true,
                'error'=>$e->getMessage()]
                ,400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
                DB::table('services')->where('service_id', $id)->delete();

                return back()->with('message', 'service deleted successfully');
        }catch(\Exception $e){
            return response()->json(
                ['status'=>true,
                'error'=>$e->getMessage()]
                ,400);
        }
    }

}
    