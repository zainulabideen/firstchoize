<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterFormRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailController;
use App\User;
use JWTAuth;use DB;
use Auth;
use Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class AuthController extends Controller
{

    public function register(Request $request) {
        $user = new User;
        $user->mobile_number = $request->mobile_number;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->save();
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }

    public function login(Request $request) {


      // $request->replace(['mobile_number' => $request->mobile_number,'password'=>$password]);
        $credentials = $request->only('mobile_number','password');

        if ( ! $token = JWTAuth::attempt($credentials)) {
                return response([
                    'status' => false,
                    'status_code' => 400,
                    'message' => 'Invalid Credentials.'
                ], 200);
        }
      
$is_admin=0; 
$is_service_provider = 0;
$is_customer = 0;
        
        

       $password = $request->password;
       if ($request->is_service_provider) {
 
      $user =DB::table('users')->where('mobile_number',$request->mobile_number)->where('is_service_provider',1)->get();
        if (count($user)>0){
      $is_service_provider=1;
       $spId=DB::select('select service_provider_id,user_id from service_provider where user_id ='.$user[0]->id);
   return response([
                'status' => true,
                'message'=>'you are login Successfully',
                'status_code'=>200,
                'token' => $token,
                'is_service_provider'=>1,
                'id'=>$user[0]->id,
                'name'=>$user[0]->name,
                'mobile_number'=>$request->mobile_number,
                'driver_id'=>$spId[0]->service_provider_id
              ]);

}else{

         
       return response([
                'status' => true,
                'message'=>'Invalid User',
                'status_code'=>200
        ]);

          }
      }elseif($request->is_customer){
        $is_customer=1;
         
           $user =DB::table('users')->where('mobile_number',$request->mobile_number)->where('is_customer',1)->get();
           if (count($user)>0){
            $custId=DB::select('select customer_id,user_id from customers  where user_id ='.$user[0]->id);

return response([
                'status' => true,
                'message'=>'you are login Successfully',
                'status_code'=>200,
                'token' => $token,
                'is_customer'=>$is_customer,
                'id'=>$user[0]->id,
                'name'=>$user[0]->name,
                'mobile_number'=>$request->mobile_number,
                'customer_id'=>$custId[0]->customer_id
        ]);
}else{

  return response([
                'status' => true,
                'message'=>'Invalid User',
                'status_code'=>200
        ]);

}
          
          Log::info($request->all());
      }else{
        $is_admin = 1;
             $user =DB::table('users')->where('mobile_number',$request->mobile_number)->where('is_admin',1)->get();


 if (count($user)>0){

  $mobile_number = $request->mobile_number;
            return response([
                'status' => true,
                'message'=>'you are login Successfully',
                'status_code'=>200,
                'token' => $token,
                'id'=>$user[0]->id,
                'name'=>$user[0]->name,
                 'mobile_number'=>$mobile_number,
                'is_admin'=>$is_admin,'is_service_provider'=>$is_service_provider,'is_customer'=>$is_customer]);
}else{

  return response([
                'status' => true,
                'message'=>'Invalid User',
                'status_code'=>200
        ]);


      }

        
    }
  }


    public function user(Request $request) {
        $user = User::find(Auth::user()->id);
        return response([
                'status' => 'success',
                'data' => $user
            ]);
    }


    public function logout() {
        JWTAuth::invalidate();
        return response([
                'status' => 'success',
                'msg' => 'Logged out Successfully.'
            ], 200);
    }

    public function refresh() {
        return response([
        'status' => 'success'
        ]);
    }

//forgotpassword
public function forgotpassword(Request $request)
{
  try{
$emp_exist=array();$driver_exist=array();
  if($request->user_type=='employee'){
    $emp_exist=DB::select('select * from employees where employees.mobile_no='.$request->mobile_no);
    if(count($emp_exist)>0){
      $user_id=$emp_exist[0]->user_id;
    }else{
      return response(['status_code' => 200,
                'status'=>false,
                'message'=>'mobile number not exist'
              ], 200);
    }
  }
  if($request->user_type=='driver'){
    $driver_exist=DB::select('select * from drivers where driver_mobile_no='.$request->mobile_no);
    if(count($driver_exist)>0){$user_id=$driver_exist[0]->user_id;}
    else{
      return response(['status_code' => 200,
                'status'=>false,
                'message'=>'mobile number not exist'
              ], 200);
    }
  }
if((count($emp_exist)>0)||(count($driver_exist)>0))
{



$password = rand(1000, 9999);

 // $message = "Your Magic Travels verification code is ##".$otp."##";
 // $otp_api_key=(env('MSG91_AUTH_KEY'));
// country=91&sender=MSGIND&route=4&mobiles=&authkey=&encrypt=&message=Hello!%20This%20is%20a%20test%20message&flash=&unicode=&schtime=&afterminutes=&response=&campaign=",

 $curl = curl_init();

 curl_setopt_array($curl, array(
   CURLOPT_URL => "http://api.msg91.com/api/sendhttp.php?country=91&sender=MGTRAV&route=4&mobiles=".$request->mobile_no."&authkey=243853Aly7FbUb6jX5bcd5eeb&message=Hi!%20Your%20Magic-Travel%20Password%20is%20".$password,
   CURLOPT_RETURNTRANSFER => true,
   CURLOPT_ENCODING => "",
   CURLOPT_MAXREDIRS => 10,
   CURLOPT_TIMEOUT => 30,
   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
   CURLOPT_CUSTOMREQUEST => "GET",
   CURLOPT_SSL_VERIFYHOST => 0,
   CURLOPT_SSL_VERIFYPEER => 0,
 ));

 $response = curl_exec($curl);
 $err = curl_error($curl);

 curl_close($curl);

 if ($err) {
   echo "cURL Error #:" . $err;
 } else {
DB::table('users')->where('id',$user_id)->update([
  'password'=>bcrypt($password)
]);
return response(['status_code' => 200,
                'status'=>true,
                'message'=>'password is sent to your mobile number'
                 ], 200);
 }

}

}
catch (\Exception $e)
        {
        return response()->json([
        'status'=>true,
        'error'=> $e->getMessage()],400);
        }

}

//reset
public function ResetPassword(Request $request)
{

  try{
  $pass_val=DB::select('select * from users where id='.$request->user_id);
  $hash=($pass_val[0]->password);
  if (password_verify($request->oldpassword, $hash)) {
  DB::table('users')->where('id',$request->user_id)->update([
    'password'=>bcrypt($request->newpassword)
  ]);
  return response(['status_code' => 200,
                  'status'=>true,
                  'message'=>'newpassword set successfully'
                   ], 200);
} else {
  return response(['status_code' => 200,
                  'status'=>false,
                  'message'=>'oldpassword not matched'
                ], 200);
}
  return "oldpassword";
}
 catch (\Exception $e)
        {
        return response()->json([
        'status'=>true,
        'error'=> $e->getMessage()],200);
        }
}

}
