<?php

namespace App\Http\Controllers\Admin;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Response;
use JWTAuth;
use Log;
use DateTime;use DateTimeZone;
use File;
use JWTAuthException;
use Carbon\Carbon;
use Session;

class Logincontroller extends Controller
{
  public function login(Request $request)
  {

    Log::info('login function');
    Log::info($request->all());
    $env=(env('APP_URL'));
    $url =$env.'/api/login';
    Log::info($url);
    $headers = array('Content-Type: application/json');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request->all()));
    $result = curl_exec($ch);
    Log::info($result);// die(print_r($request->all()));
    $json = json_decode(utf8_encode($result), true);
     if(!isset($json["token"])){
      return back()->with('invalidCred', 'Invalid email or password.');
    }
    $token=$json['token'];
    $is_admin=$json['is_admin'];
    Log::info($token);
       if(!empty($token) && ($is_admin==1))
            {
              Log::info($token);
               Session::put('admin_token', $token);
               Session::put('is_admin',$is_admin);
               Session::save();
              return redirect('/dashboard');
            }
        else
            {
                Session::flush();
              return back()->with('warning', 'You are not Admin.');
            }
   }

//logout
public function logout(Request $request)
   {

       Log::info('logout function');
       Log::info($request->all());
       Session::flush();
       Log::info(Session::get('admin_token'));
      return redirect('/')->with('warning', 'You have Logged out!');
   }
}
