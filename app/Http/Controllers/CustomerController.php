<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\Exceptions\JWTException;
use Response;
use JWTAuth;
use Log;
use File;
use JWTAuthException;
use Carbon\Carbon;


class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('store in CustomerController');
        try{
          Log::info($request->all());
          $user_id=DB::table('users')->insertGetId([
            'name'=>$request->first_name . "" . $request->last_name,
            'mobile_number'=>$request->mobile_number,
            'password'=>bcrypt('12345'),
            'is_customer'=>1,
            'registered_date'=>Carbon::now(),
            'created_at'=>Carbon::now()
          ]);
   
          $employee_id=DB::table('customers')->insertGetId([
            'user_id'=>$user_id,
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'mobile_number'=>$request->mobile_number,
           //  'gender'=>$request->gender,
           //  'password'=>$request->password,
            'email_id'=>$request->email_id,
            'city'=>$request->city,
           //  'country_name'=>$request->country_name,
            'country_code'=>$request->country_code,
           //  'device_type'=>$request->device_type,
           //  'device_token'=>$request->device_token,
           //  'customer_status'=>$request->customer_status,
           //  'customer_image'=>$request->customer_image,
           //  'customer_lat'=>$request->customer_lat,
           //  'customer_long'=>$request->customer_long,
           //  'app_version_ios'=>$request->app_version_ios,
           //  'app_version_android'=>$request->app_version_android,
           //  'address'=>$request->address,
           //  'app_version_ios'=>$request->app_version_ios,
           //  'app_version_ios'=>$request->app_version_ios,
           //  'app_version_ios'=>$request->app_version_ios,
            'created_at'=>Carbon::now() 
          ]);
   //       return back()->with('message','Customer insert successfully');
   
          return response(['status_code' => 200,
                          'status'=>true,
                          'message'=>'Customer inserted successfully'
                           ], 200);
        }
        catch (\Exception $e)
            {
            return response()->json([
            'status'=>false,
            'status_code'=>400,
            'message'=>'try agian later...',
            'error'=> $e->getMessage()],200);
            }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
