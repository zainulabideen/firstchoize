
@include('partials.navbar')
<body class="hold-transition skin-purple sidebar-mini" id="fullscreen">
  <style>
  #message {
    -moz-animation: cssAnimation 0s ease-in 6s forwards;
    /* Firefox */
    -webkit-animation: cssAnimation 0s ease-in 6s forwards;
    /* Safari and Chrome */
    -o-animation: cssAnimation 0s ease-in 6s forwards;}
  </style>
    <!-- Site wrapper -->
    <div class="wrapper">
 @include('partials.asidebar')
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->

            @if (session('message'))
            <div class="w-form-fail" id="message" style="background: green;font-size: 25px;color: white;">
            <div> {{session('message')}} </div>
          </div>  @endif

              <section class="content-header">
                <h1>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Shift Creation</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="head">
                            <h1 class="pull-left" >Shift Creation</h1>
                            <!-- <button type="button" class="pull-right button raised primary" data-toggle="modal" data-target="#addNew">Add New</button> -->
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-12">
                        <div class="md-button-box">
                            <div class="btn-group">
                                <button class="md-button" data-toggle="modal" data-target="#addNew" onclick="myfunction('addNew')">Add New</button>
                                <button class="md-button" data-toggle="modal" data-target="#edit" onclick="myfunction('edit')">Edit</button>
                                <button class="md-button">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form  method="POST" name='shift_form'  id="shift_creation">{{csrf_field()}}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="background">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Shift Name</label>
                                        <input type="text" id="shift_name" name="shift_name" class="form-control" placeholder="Shift Name" />
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group bootstrap-timepicker">
                                        <label>Shift Time From</label>
                                        <input type="text" name="shift_time_from"  id="shift_time_from" class="form-control timepicker" placeholder="Shift Time From" />
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group bootstrap-timepicker">
                                        <label>Shift Time To</label>
                                        <input type="text" name="shift_time_to" id="shift_time_to" class="form-control timepicker" placeholder="Shift Time To" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><input type="hidden" name='shift_id' id='shift_id'/>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="background">
                                <button type="submit" id='save_btn' class="md-button pull-right">Create Shift</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="table-responsive shadow-z-1">
                                <table class="table" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>
                                            <th>Shift Name</th>
                                            <th>Shift Start Time</th>
                                            <th>Shift End Time</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($shiftcreation as $key=>$shift)  <tr>
                                        <td>{{$key+1}}</td>
                                            <td id='sname_{{$shift->shift_id}}'>{{$shift->shift_name}} </td>
                                            <td id='shift_time_from_{{$shift->shift_id}}'>{{$shift->shift_time_from}}</td>
                                            <td id='shift_time_to_{{$shift->shift_id}}'>{{$shift->shift_time_to}}</td>
                                            <td>
                                                <button class="btn btn-warning btn-xs delete" onclick="onclickedit({{$shift->shift_id}})"><i class="fa fa-pencil"></i></button>
                                                <a class="btn btn-danger btn-xs delete" data-toggle="modal"  onclick="onclickdelete({{$shift->shift_id}})"  class="btn btn-info" data-target="#myModal2"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section> <!-- /.content -->
        </div><!-- /.content-wrapper -->


        <div class="modal fade slide-up disable-scroll" id="myModal2" tabindex="-1" role="dialog" display="block" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
              </button>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h3>   <span class="semi-bold">Delete Shift</span></h3>

            </div>
            <div class="panel-body">
          <div class="row">
            <div class="col-sm-10">

        <form class="form-horizontal" method="POST" action="/deleteshift" role="form" enctype="multipart/form-data" >{{ csrf_field() }}
       <div class="box-body" >
     <input type='hidden' id='delete_shift_id' name='del_shift_id'/>
      Are u sure you want to delete Shift?




         <div class="box-footer">

                  <button type="submit" id='save_btn' class="btn btn-success pull-right" id='edit_save_btn'>Delete</button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-right:25%">Cancel</button>
                </div>

       </div>
       </div></form>
            </div>
          </div>
        </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>






        <!-- Modal Popup for Add New Starts -->
            <div class="modal fade" id="addNew" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Add Office</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="#" method="POST" id="add_office">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label" >Branch Name <span class="red">*</span> </p>
                                        <input type="text" name="branchname" class="form-control" placeholder="Branch Name" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label" >Branch ID <span class="red">*</span></p>
                                        <input type="text" name="branchid" class="form-control" placeholder="Branch ID" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Head Office <span class="red">*</span></p>
                                        <select class="form-control select2" name="headoffice" style="width: 100%;">
                                            <option selected="selected" value="">Select Head Office</option>
                                            <option>Office One</option>
                                            <option>Office Two</option>
                                            <option>Office Three</option>
                                            <option>Office Four</option>
                                            <option>Office Five</option>
                                            <option>Office Six</option>
                                            <option>Office Seven</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Contact Person <span class="red">*</span></p>
                                        <select class="form-control select2" name="contactperson" style="width: 100%;">
                                            <option selected="selected" value="">Select Contact Person</option>
                                            <option>Person One</option>
                                            <option>Person Two</option>
                                            <option>Person Three</option>
                                            <option>Person Four</option>
                                            <option>Person Five</option>
                                            <option>Person Six</option>
                                            <option>Person Seven</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label">Address <span class="red">*</span></p>
                                        <input type="text" name="address" class="form-control" placeholder="Address" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Phone Number <span class="red">*</span></p>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Fax Number</p>
                                        <input type="text" name="fax" class="form-control" placeholder="Fax Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Pincode <span class="red">*</span></p>
                                        <input type="text" name="pincode" class="form-control" placeholder="Pincode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="button raised primary pull-right">Add</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Add New Ends -->

        <!-- Modal Popup for Edit Office Starts -->
            <div class="modal fade" id="edit" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Edit Shift Creation</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="#" method="POST" id="edit_office">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label" >Branch Name</p>
                                        <input type="text" name="branchname" class="form-control" placeholder="Branch Name" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label" >Branch ID</p>
                                        <input type="text" name="branchid" class="form-control" placeholder="Branch ID" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Head Office</p>
                                        <select id="one" class="form-control select2" name="headoffice" style="width: 100%;">
                                            <option selected="selected" value="">Select Head Office</option>
                                            <option>Office One</option>
                                            <option>Office Two</option>
                                            <option>Office Three</option>
                                            <option>Office Four</option>
                                            <option>Office Five</option>
                                            <option>Office Six</option>
                                            <option>Office Seven</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Contact Person</p>
                                         <select class="form-control select2" name="contactperson" style="width: 100%;">
                                            <option selected="selected" value="">Select Contact Person</option>
                                            <option>Person One</option>
                                            <option>Person Two</option>
                                            <option>Person Three</option>
                                            <option>Person Four</option>
                                            <option>Person Five</option>
                                            <option>Person Six</option>
                                            <option>Person Seven</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label">Address</p>
                                        <input type="text" name="address" class="form-control" placeholder="Address" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Phone Number</p>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Fax Number</p>
                                        <input type="text" name="" class="form-control" placeholder="Fax Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Pincode</p>
                                        <input type="text" name="pincode" class="form-control" placeholder="Pincode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="button raised primary pull-right">Update</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Edit Office Ends -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.3
            </div>
            <strong>Copyright &copy; 2018-2019 <a href="#">Magic Travels</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
   <!-- jQuery 2.2.0 -->
    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/form-validator.js"></script>
    <script type="text/javascript" src="dist/js/custom-validation.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <script src="dist/js/default.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
    $("#message").fadeIn( 1000 ).delay( 1000 ).fadeOut( 1000 );});
     $('#shift_creation_li').addClass('active');
        function myfunction(x)
        {
            ModalPass(x);
        }



    $(function () {
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: true
        });
      });
     $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
function onclickedit(id)
{$('#save_btn').html('edit');
  $('#shift_id').val(id);
  var sname=$('#sname_'+id).html();
  var shift_time_from=$('#shift_time_from_'+id).html();
  var shift_time_to =$('#shift_time_to_'+id).html();
  console.log(sname);
$('#shift_name').val(sname);
$('#shift_time_from').val(shift_time_from);
$('#shift_time_to').val(shift_time_to);
}

function onclickdelete(id)
{
  $('#delete_shift_id').val(id);

}

    $('#shift_creation').click(function()
      {
        var shift_id=$('#shift_id').val();
      var save=  $('#save_btn').html();
      var id=  $('#shift_id').val();
      if(save == 'edit'){
        document.shift_form.action ="updateshift_creation/"+id;
      }else{
        document.shift_form.action ="shift_creation";
      }

     })
    </script>
</body>

</html>
