@include('partials.navbar')

<body class="hold-transition skin-purple sidebar-mini" id="fullscreen">
  <style>
  #message {
    -moz-animation: cssAnimation 0s ease-in 6s forwards;
    /* Firefox */
    -webkit-animation: cssAnimation 0s ease-in 6s forwards;
    /* Safari and Chrome */
    -o-animation: cssAnimation 0s ease-in 6s forwards;}
  </style>
      <!-- Site wrapper -->
    <div class="wrapper">
@include('partials.asidebar')
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          @if (session('message'))
          <div class="w-form-fail" id="message" style="background: green;font-size: 25px;color: white;">
          <div> {{session('message')}} </div>
        </div>  @endif
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active"> Department</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="head">
                            <h1 class="pull-left" > Department</h1>
                            <!-- <button type="button" class="pull-right button raised primary" data-toggle="modal" data-target="#addNew">Add New</button> -->
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-12">
                        <div class="md-button-box">
                            <div class="btn-group">
                                <button class="md-button" data-toggle="modal" data-target="#addNew" onclick="myfunction('addNew')">Add New</button>
                                <button class="md-button" data-toggle="modal" data-target="#edit" onclick="myfunction('edit')">Edit</button>
                                <button class="md-button">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="department" id='department' method="POST" >{{csrf_field()}}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="background">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Department Name</label>
                                        <input type="text" required="" name="dept_name" id='dept_name' class="form-control" placeholder="Department Name" />
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group" style="margin-top:21px;">
                                        <button  type='submit' class="md-button">Create Department</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="table-responsive shadow-z-1">
                                <table class="table" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>
                                            <th>Department</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($department as $key=>$dept)  <tr>
                                            <td>{{$key+1}}. </td>
                                            <td id='dept_{{$dept->department_id}}'>{{$dept->department_name}}</td>
                                            <td>
                                              <a class="btn btn-warning btn-xs delete" class="btn btn-info"   data-toggle="modal" data-target="#myModal2"  onclick="onclickedit({{$dept->department_id}})"><i class="fa fa-pencil"></i></button>
                                              <a class="btn btn-danger btn-xs delete" data-toggle="modal"  onclick="onclickdelete({{$dept->department_id}})"  class="btn btn-info" data-target="#myModal3"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>@endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section> <!-- /.content -->
        </div><!-- /.content-wrapper -->

        <!-- Modal Popup for Add New Starts -->

        <!-- Modal Popup for Add New Ends -->

        <!-- Modal Popup for Edit Office Starts -->
            <div class="modal fade" id="myModal2" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Edit Department</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="updatedepartment" method="POST" id="edit_office">{{csrf_field()}}
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                      <input type='hidden' id='department_id' name='department_id'/>
                                        <p class="form-label" >Department Name</p>
                                        <input type="text" required name="department_name" id='department_name' class="form-control" placeholder="Department Name" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <button class="button raised primary pull-right">Update</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade slide-up disable-scroll" id="myModal3" tabindex="-1" role="dialog" display="block" aria-hidden="false">
          <div class="modal-dialog ">
            <div class="modal-content-wrapper">
              <div class="modal-content">
                <div class="modal-header clearfix text-left">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                  </button>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                  <h3>   <span class="semi-bold">Delete Department</span></h3>

                </div>
                <div class="panel-body">
              <div class="row">
                <div class="col-sm-10">

            <form class="form-horizontal" method="POST" action="/deletedepartment" role="form" enctype="multipart/form-data" >{{ csrf_field() }}
           <div class="box-body" >
         <input type='hidden' id='del_department_id' name='del_department_id'/>
          Are u sure you want to delete Dedpartment?




             <div class="box-footer">

                      <button type="submit" id='save_btn' class="btn btn-success pull-right" id='edit_save_btn'>Delete</button>
                         <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-right:25%">Cancel</button>
                    </div>

           </div>
           </div></form>
                </div>
              </div>
            </div>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>

        <!-- Modal Popup for Edit Office Ends -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.3
            </div>
            <strong>Copyright &copy; 2018-2019 <a href="#">Magic Travels</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
   <!-- jQuery 2.2.0 -->
    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/form-validator.js"></script>
    <script type="text/javascript" src="dist/js/custom-validation.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <script src="dist/js/default.js"></script>
    <script type="text/javascript">
       $('#department_li').addClass('active');
        function myfunction(x)
        {
            ModalPass(x);
        }



    $(function () {
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: true
        });
      });
     $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
      function onclickedit(id)
      {
          var dept=$('#dept_'+id).text();
          $('#department_id').val(id);
          $('#department_name').val(dept);
      }

      function onclickdelete(id)
      {
        $('#del_department_id').val(id);

      }
      $(document).ready(function(){
      $("#message").fadeIn( 1000 ).delay( 1000 ).fadeOut( 1000 );});
    </script>
</body>

</html>
