@include('partials.navbar')
<body class="hold-transition skin-purple sidebar-mini" id="fullscreen">
    <!-- Site wrapper -->
    <div class="wrapper">
   @include('partials.asidebar')
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Trip Scheduled</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="head">
                            <h1 class="pull-left">Trip Scheduled</h1>
                            <!-- <button type="button" class="pull-right button raised primary" data-toggle="modal" data-target="#addNew">Add New</button> -->
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-12">
                        <div class="md-button-box">
                            <div class="btn-group">
                                <button class="md-button" data-toggle="modal" data-target="#addNew" onclick="myfunction('addNew')">Add New</button>
                                <button class="md-button" data-toggle="modal" data-target="#edit" onclick="myfunction('edit')">Edit</button>
                                <button class="md-button">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="table-responsive shadow-z-1">
                                <table class="table" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>
                                            <th>Trip Date</th>
                                            <th>Office Time</th>
                                            <th>Drive Name</th>
                                            <th>Vehicle Number</th>
                                            <th>Type</th>
                                            <th>Trip Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($tripscheduled as $key=>$trip)
                                      <tr>
                                      <td style="text-align:center;">{{$key+1}}</td>
                                            <td style="text-align:center;">{{date("d-m-Y", strtotime($trip->trip_created_date))}}</td>
                                            <td style="text-align:center;"> {{$trip->shift_time_from}}{{$trip->shift_time_to}}</td>
                                            <td style="text-align:center;">{{$trip->driver_name}}</td>
                                            <td style="text-align:center;">{{$trip->vehicle_number}}</td>
                                            <td style="text-align:center;">{{$trip->trip_type}}</td>
                                            <td style="text-align:center;">{{$trip->type}}</td>
                                            <td style="text-align:center;"><button class="btn btn-success btn-xs delete" data-toggle="modal" onclick="onclickSaveToTripSheet({{$trip->trip_id}})" data-target="#addNew">stop/save</button></td>                                      </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section> <!-- /.content -->
        </div><!-- /.content-wrapper -->

        <!-- Modal Popup for Add New Starts -->
            <div class="modal fade" id="addNew" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Stop trip and send to TripSheet</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="/sendtotripsheet" method="POST" id="add_office">

                                  {{csrf_field()}}


                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div class="form-group">

                                        <input type="hidden" name="trip_id" id='trip_id' class="form-control"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type='submit' class="button raised primary pull-right">OK</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Add New Starts -->
            <div class="modal fade" id="addNew" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Add Office</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="#" method="POST" id="add_office">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label" >Branch Name <span class="red">*</span> </p>
                                        <input type="text" name="branchname" class="form-control" placeholder="Branch Name" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label" >Branch ID <span class="red">*</span></p>
                                        <input type="text" name="branchid" class="form-control" placeholder="Branch ID" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Head Office <span class="red">*</span></p>
                                        <select class="form-control select2" name="headoffice" style="width: 100%;">
                                            <option selected="selected" value="">Select Head Office</option>
                                            <option>Office One</option>
                                            <option>Office Two</option>
                                            <option>Office Three</option>
                                            <option>Office Four</option>
                                            <option>Office Five</option>
                                            <option>Office Six</option>
                                            <option>Office Seven</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Contact Person <span class="red">*</span></p>
                                        <select class="form-control select2" name="contactperson" style="width: 100%;">
                                            <option selected="selected" value="">Select Contact Person</option>
                                            <option>Person One</option>
                                            <option>Person Two</option>
                                            <option>Person Three</option>
                                            <option>Person Four</option>
                                            <option>Person Five</option>
                                            <option>Person Six</option>
                                            <option>Person Seven</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label">Address <span class="red">*</span></p>
                                        <input type="text" name="address" class="form-control" placeholder="Address" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Phone Number <span class="red">*</span></p>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Fax Number</p>
                                        <input type="text" name="fax" class="form-control" placeholder="Fax Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Pincode <span class="red">*</span></p>
                                        <input type="text" name="pincode" class="form-control" placeholder="Pincode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="button raised primary pull-right">Add</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Add New Ends -->

        <!-- Modal Popup for Edit Office Starts -->
            <div class="modal fade" id="edit" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Edit Office</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="#" method="POST" id="edit_office">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label" >Branch Name</p>
                                        <input type="text" name="branchname" class="form-control" placeholder="Branch Name" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label" >Branch ID</p>
                                        <input type="text" name="branchid" class="form-control" placeholder="Branch ID" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Head Office</p>
                                        <select id="one" class="form-control select2" name="headoffice" style="width: 100%;">
                                            <option selected="selected" value="">Select Head Office</option>
                                            <option>Office One</option>
                                            <option>Office Two</option>
                                            <option>Office Three</option>
                                            <option>Office Four</option>
                                            <option>Office Five</option>
                                            <option>Office Six</option>
                                            <option>Office Seven</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Contact Person</p>
                                         <select class="form-control select2" name="contactperson" style="width: 100%;">
                                            <option selected="selected" value="">Select Contact Person</option>
                                            <option>Person One</option>
                                            <option>Person Two</option>
                                            <option>Person Three</option>
                                            <option>Person Four</option>
                                            <option>Person Five</option>
                                            <option>Person Six</option>
                                            <option>Person Seven</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label">Address</p>
                                        <input type="text" name="address" class="form-control" placeholder="Address" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Phone Number</p>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Fax Number</p>
                                        <input type="text" name="" class="form-control" placeholder="Fax Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Pincode</p>
                                        <input type="text" name="pincode" class="form-control" placeholder="Pincode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="button raised primary pull-right">Update</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Edit Office Ends -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.3
            </div>
            <strong>Copyright &copy; 2018-2019 <a href="#">Magic Travels</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
   <!-- jQuery 2.2.0 -->
  <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/form-validator.js"></script>
    <script type="text/javascript" src="dist/js/custom-validation.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <script src="dist/js/default.js"></script>
<script src='https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js'></script>
<script src='https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js'></script>
    <script type="text/javascript">
        function myfunction(x)
        {
            ModalPass(x);
        }
        function onclickSaveToTripSheet(id)
           {
             $('#trip_id').val(id);

           }
 $('#trip_scheduled_li').addClass('active');


     $(document).ready(function() {
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
         {
           extend: 'pdfHtml5',
           title:' Trip Scheduled',
           orientation: 'landscape',
           pageSize: 'LEGAL',
           exportOptions: {
               columns: [0, 1, 2, 3, 4, 5, 6]
           }

         },
         {
           extend: 'excelHtml5',
           title:' Trip Scheduled',
           orientation: 'landscape',
           pageSize: 'LEGAL',
           exportOptions: {
               columns: [0, 1, 2, 3, 4, 5, 6]
           }

         },
           {
           extend: 'print',
           title:' Trip Scheduled',
           orientation: 'landscape',
           pageSize: 'LEGAL',
           exportOptions: {
               columns: [0, 1, 2, 3, 4, 5, 6]
           }

         }

       ]
    } );
} );

    </script>
</body>

</html>
