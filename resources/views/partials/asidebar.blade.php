<header class="main-header">
    <a href="dashboard" class="logo">
        <span class="logo-mini"><b>FC</b></span>
        <span class="logo-lg"><b>First Choize </b></span>
    </a>
    <nav class="navbar navbar-static-top">
        <div class="search-bar">
            <input type="text" name="" placeholder="Search...." class="header-search" />
            <label for="email" class="glyphicon glyphicon-search" rel="tooltip" title="email"></label>
            <div class="alert-close">×</div>
        </div>
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="search_bar">
                        <i class="fa fa-search"></i>
                    </a>
                </li>
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle requestfullscreen">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" class="dropdown-toggle exitfullscreen" style="display: none">
                        <i class="fa fa-compress"></i>
                    </a>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/car.jpg" class="user-image" alt="User Image">
                        <span class="hidden-xs">ADMIN</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="/car.jpg" class="img-circle" alt="User Image">
                            <p>
                                 - Admin
                                <small>First Choize </small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="profile" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="logout" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- =============================================== -->
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/car.jpg" class="img-circle" style='height:63px;' alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Admin</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li id='dashboard_li'><a href="dashboard"><i class="ion ion-speedometer"></i> <span>Dashboard</span></a></li>
            <li id='company_register_li'><a href="add_service"><i class="ion ion-model-s"></i> <span>Add Services</span></a></li>
            <!--<li id='department_li'><a href="department"><i class="ion ion-model-s"></i> <span>Department</span></a></li>
            <li id='shift_creation_li' ><a href="shift_creation"><i class="ion ion-model-s"></i> <span>Shift Creation</span></a></li>
            <li id='route_management_li' ><a href="route_management"><i class="ion ion-ios-navigate"></i> <span>Route Management</span></a></li>
            <li id='employee_register_li'><a href="employee_register"><i class="ion ion-person"></i> <span>Employee Register</span></a></li>
            <li id=''><a href="driver_register"><i class="ion ion-ios-person"></i> <span>Driver Register</span></a></li>
            <li id='vehicle_register_li'><a href="vehicle_register"><i class="ion ion-model-s"></i> <span>Vehicle Register</span></a></li>
            <li id='escort_register_li'><a href="escort_register"><i class="ion ion-locked"></i> <span>Escort Register</span></a></li>
            <li id='driver_vehicle_merge_li'><a href="driver_vehicle_merge"><i class="ion ion-person"></i> <span>Driver Vehicle Merge</span></a></li>
            <li id='swap_vehicle_li'><a href="swap_vehicle"><i class="ion ion-arrow-swap"></i> <span>Swap Vehicle</span></a></li>
            <li id='extra_cab_request_li'><a href="extra_cab_request"><i class="ion ion-model-s"></i> <span>Extra Cab Request</span></a></li>
            <li id='adhoc_request_li'><a href="/adhoc_request"><i class="ion ion-clipboard"></i> <span>Adhoc Request</span></a></li>
            <li id='allocate_vehicle_li'><a href="allocate_vehiclelogin"><i class="ion ion-model-s"></i> <span>Allocate Vehicle</span></a></li>
            <li id='allocate_extra_cab_li'><a href="allocate_extra_cab"><i class="ion ion-model-s"></i> <span>Allocate Extra Cab</span></a></li>
            <li id='trip_sheet_li'><a href="trip_sheet"><i class="ion ion-map"></i> <span>Trip Sheet</span></a></li>
            <li id='trip_logs_li'><a href="trip_logs"><i class="ion ion-map"></i> <span>Trip Logs</span></a></li>
            <li id='trip_progress_li'><a href="trip_progress"><i class="ion ion-map"></i> <span>Trip In Progress</span></a></li>
            <li id='trip_scheduled_li'> <a href="trip_scheduled"><i class="ion ion-clock"></i> <span>Scheduled Trips</span></a></li> -->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
