company_address
@include('partials.navbar')
<body class="hold-transition skin-purple sidebar-mini" id="fullscreen">
    <!-- Site wrapper -->
    <div class="wrapper">
 @include('partials.asidebar')
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        @if (session('message'))
            <div class="w-form-fail" id="message" style="background: green;font-size: 25px;color: white;">
            <div> {{session('message')}} </div>
          </div>  @endif
            <section class="content-header">
                <h1>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">company Register</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="head">
                            <h1 class="pull-left" >company Register</h1>
                            <!-- <button type="button" class="pull-right button raised primary" data-toggle="modal" data-target="#addNew">Add New</button> -->
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-12">
                        <div class="md-button-box">
                            <div class="btn-group">
                                <button class="md-button" data-toggle="modal" data-target="#addNew" onclick="myfunction('addNew')">Add New</button>
                                <button class="md-button" data-toggle="modal" data-target="#edit" onclick="myfunction('edit')">Edit</button>
                                <button class="md-button">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form name="company_register" method="POST" id="company_register">{{csrf_field()}}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="background">

                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Company Name</label>
                                            <input type="text" id = "company_name" required name="company_name" class="form-control" placeholder="Company Name" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Company Branch</label>
                                            <input type="text" id = "company_branch" required name="company_branch" class="form-control" placeholder="Company Branch" />
                                        </div>
                                    </div>
                                    <input type="hidden" name='company_id' id='company_id'/>

                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Company Address</label>
                                            <input  required id = "company_address" name="company_address" class="form-control" placeholder="Company Address" />
                                        </div>
                                    </div>
                                    <input id='company_lat' name='company_lat'  class="form-control" style="width: 100%;" type="hidden"/>
                                    <input id='company_long' name='company_long'  class="form-control" style="width: 100%;" type="hidden"/>

                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Company SOS</label>
                                            <input  type='number' required id = "company_sos" name="company_sos" class="form-control" placeholder="Company SOS" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group" style="margin-top:21px;">
                                            <button type="submit" id="save_btn" class="md-button">Register</button>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-xs-12">
                      <!-- <input id="pac-input" type="text"
                      placeholder="Enter a location"> -->


                      <div id="map" style="width:100%;height:250px;"></div>

                      <div id="company_address" ></div>
                      <input id="address" type="hidden" value="Banglore">
                        <!-- <div class="googleMap">

                            <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15551.277544019764!2d77.57913564999998!3d12.98340115!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1538394480216" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div> -->



                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="table-responsive shadow-z-1">
                                <table class="table" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>
                                            <th>Company Name</th>
                                            <th>Company Branch</th>
                                            <th>Company Address</th>
                                             <th>Company SOS</th>
                                            <th>action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($company as $key=>$cmp)
                                      <tr>
                                      <td>{{$key+1}}</td>

                                            <td id='comp_name_{{$cmp->company_branch_id}}'>{{$cmp->company_name}}</td>
                                            <td id='comp_branch_{{$cmp->company_branch_id}}'>{{$cmp->company_branch_name}}</td>
                                            <td id='comp_address_{{$cmp->company_branch_id}}'>{{$cmp->company_address}}</td>
                                             <td id='company_sos_{{$cmp->company_branch_id}}'>{{$cmp->company_sos}}</td>
                                            <td>
                                                <button class="btn btn-warning btn-xs delete" onclick="onclickedit({{$cmp->company_branch_id}})"><i class="fa fa-pencil"></i></button>
                                                <button class="btn btn-danger btn-xs delete" data-toggle="modal" onclick="onclickdelete({{$cmp->company_branch_id}})" data-target="#myModal3"><i class="fa fa-trash"></i></button>
                                            </td>
                                          </tr>
                          @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section> <!-- /.content -->
        </div><!-- /.content-wrapper -->

        <div class="modal fade slide-up disable-scroll" id="myModal3" tabindex="-1" role="dialog" display="block" aria-hidden="false">
              <div class="modal-dialog ">
                <div class="modal-content-wrapper">
                  <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                      </button>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                      <h3>   <span class="semi-bold">Delete company</span></h3>

                    </div>
                    <div class="panel-body">
                  <div class="row">
                    <div class="col-sm-10">

                <form class="form-horizontal" method="POST" action="/delete_company_register" role="form" enctype="multipart/form-data" >{{ csrf_field() }}
               <div class="box-body" >
             <input type='hidden' id='delete_company_id' name='delete_company_id'/>
              Are u sure you want to delete company?




                 <div class="box-footer">

                          <button type="submit" id='save_btn' class="btn btn-success pull-right" id='edit_save_btn'>Delete</button>
                             <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-right:25%">Cancel</button>
                        </div>

               </div>
               </div></form>
                    </div>
                  </div>
                </div>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
        <!-- Modal Popup for Add New Starts -->
            <div class="modal fade" id="addNew" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Add Office</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="" method="POST" id="add_office">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label" >Branch Name <span class="red">*</span> </p>
                                        <input type="text" name="branchname" class="form-control" placeholder="Branch Name" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label" >Branch ID <span class="red">*</span></p>
                                        <input type="text" name="branchid" class="form-control" placeholder="Branch ID" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Head Office <span class="red">*</span></p>
                                        <select class="form-control select2" name="headoffice" style="width: 100%;">
                                            <option selected="selected" value="">Select Head Office</option>
                                            <option>Office One</option>
                                            <option>Office Two</option>
                                            <option>Office Three</option>
                                            <option>Office Four</option>
                                            <option>Office Five</option>
                                            <option>Office Six</option>
                                            <option>Office Seven</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Contact Person <span class="red">*</span></p>
                                        <select class="form-control select2" name="contactperson" style="width: 100%;">
                                            <option selected="selected" value="">Select Contact Person</option>
                                            <option>Person One</option>
                                            <option>Person Two</option>
                                            <option>Person Three</option>
                                            <option>Person Four</option>
                                            <option>Person Five</option>
                                            <option>Person Six</option>
                                            <option>Person Seven</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label">Address <span class="red">*</span></p>
                                        <input type="text" name="address" class="form-control" placeholder="Address" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Phone Number <span class="red">*</span></p>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Fax Number</p>
                                        <input type="text" name="fax" class="form-control" placeholder="Fax Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Pincode <span class="red">*</span></p>
                                        <input type="text" name="pincode" class="form-control" placeholder="Pincode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="button raised primary pull-right">Add</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Add New Ends -->

        <!-- Modal Popup for Edit Office Starts -->
            <div class="modal fade" id="edit" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Edit Office</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="#" method="POST" id="edit_office">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label" >Branch Name</p>
                                        <input type="text" name="branchname" class="form-control" placeholder="Branch Name" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label" >Branch ID</p>
                                        <input type="text" name="branchid" class="form-control" placeholder="Branch ID" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Head Office</p>
                                        <select id="one" class="form-control select2" name="headoffice" style="width: 100%;">
                                            <option selected="selected" value="">Select Head Office</option>
                                            <option>Office One</option>
                                            <option>Office Two</option>
                                            <option>Office Three</option>
                                            <option>Office Four</option>
                                            <option>Office Five</option>
                                            <option>Office Six</option>
                                            <option>Office Seven</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Contact Person</p>
                                         <select class="form-control select2" name="contactperson" style="width: 100%;">
                                            <option selected="selected" value="">Select Contact Person</option>
                                            <option>Person One</option>
                                            <option>Person Two</option>
                                            <option>Person Three</option>
                                            <option>Person Four</option>
                                            <option>Person Five</option>
                                            <option>Person Six</option>
                                            <option>Person Seven</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label">Address</p>
                                        <input type="text" name="address" class="form-control" placeholder="Address" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Phone Number</p>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Fax Number</p>
                                        <input type="text" name="" class="form-control" placeholder="Fax Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Pincode</p>
                                        <input type="text" name="pincode" class="form-control" placeholder="Pincode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="button raised primary pull-right">Update</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Edit Office Ends -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.3
            </div>
            <strong>Copyright &copy; 2018-2019 <a href="#">Magic Travels</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
   <!-- jQuery 2.2.0 -->
    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/form-validator.js"></script>
    <script type="text/javascript" src="dist/js/custom-validation.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <script src="dist/js/default.js"></script>
    <script type="text/javascript">
         $('#company_register_li').addClass('active');
        function myfunction(x)
        {
            ModalPass(x);
        }



    $(function () {
        $(".select2").select2();
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: true
        });
      });
     $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });

function onclickedit(id)
{$('#save_btn').html('edit');
  $('#company_id').val(id);
  var comp_name=$('#comp_name_'+id).html();
  var comp_branch=$('#comp_branch_'+id).html();
  var comp_address =$('#comp_address_'+id).html();
   var company_sos =$('#company_sos_'+id).html();

  // console.log(sname);
$('#company_name').val(comp_name);
$('#company_address').val(comp_address);
$('#company_branch').val(comp_branch);
$('#company_sos').val(company_sos);

}

function onclickdelete(id)
{
  $('#delete_company_id').val(id);

}

    $('#save_btn').click(function()
      {


      var save=  $('#save_btn').html();
      var id=  $('#company_id').val();
      if(save == 'edit'){

        document.company_register.action ="update_company_register/"+id;
      }else{
        document.company_register.action ="company_register";
      }

     })

            //Here is my logic now

     var geocoder;
     var map;
     var marker;
     var infowindow = new google.maps.InfoWindow({size: new google.maps.Size(150,50)});
     function initMap() {
     geocoder = new google.maps.Geocoder();
     var latlng = new google.maps.LatLng(12.9716, -77.5946);
     var mapOptions = {
     zoom:6,
     center: latlng,
     mapTypeId: google.maps.MapTypeId.ROADMAP
     }

     map = new google.maps.Map(document.getElementById('map'), mapOptions);

     var input = document.getElementById('company_address');

     var autocomplete = new google.maps.places.Autocomplete(input);

     autocomplete.bindTo('bounds', map);

     var address = document.getElementById('address').value;
     geocoder.geocode( { 'address': address}, function(results, status) {
     if (status == google.maps.GeocoderStatus.OK) {
     map.setCenter(results[0].geometry.location);
     if (marker) {
     marker.setMap(null);
     if (infowindow) infowindow.close();
     }
     marker = new google.maps.Marker({
     map: map,
     draggable: true,
     position: results[0].geometry.location
     });
     google.maps.event.addListenerOnce(map, 'idle', function () {

     google.maps.event.trigger(map, 'resize');

     });
     google.maps.event.addListener(marker, 'dragend', function() {

     geocodePosition(marker.getPosition());
     });

     google.maps.event.trigger(marker, 'click');
     } else {
     alert('Geocode was not successful for the following reason: ' + status);
     }
     });

     autocomplete.addListener('place_changed', function() {

     marker.setVisible(false);
     var place = autocomplete.getPlace();
     if (!place.geometry) {
     window.alert("No details available for input: '" + place.name + "'");
     return;
     }

     if (place.geometry.viewport) {
     map.fitBounds(place.geometry.viewport);
     } else {
     map.setCenter(place.geometry.location);
     map.setZoom(17); // Why 17? Because it looks good.
     }
     marker.setPosition(place.geometry.location);
     marker.setVisible(true);
     $('#company_lat').val(marker.getPosition().lat());
     $('#company_long').val(marker.getPosition().lng());
     // alert(marker.getPosition().lng());

     var address = '';
     if (place.address_components) {
     address = [
     (place.address_components[0] && place.address_components[0].short_name || ''),
     (place.address_components[1] && place.address_components[1].short_name || ''),
     (place.address_components[2] && place.address_components[2].short_name || ''),
     (place.address_components[3] && place.address_components[3].short_name || ''),
     (place.address_components[4] && place.address_components[4].short_name || ''),
     (place.address_components[5] && place.address_components[5].short_name || '')
     ].join(' ');
     }

     document.getElementById('company_address').value=place.name+', '+address;

     });

     }

     function geocodePosition(pos) {
     geocoder.geocode({
     latLng: pos
     }, function(responses) {
     if (responses && responses.length > 0) {
     marker.formatted_address = responses[0].formatted_address;

     } else {
     marker.formatted_address = 'Cannot determine address at this location.';
     }

     document.getElementById('company_address').value=marker.formatted_address;
     $('#company_lat').val(marker.getPosition().lat());
     $('#company_long').val(marker.getPosition().lng());
     // alert(marker.getPosition().lng());
     });
     }




    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAl_ZSipciqQS8XLebSVN7FfvOek8tnlt0&libraries=places&callback=initMap"
    async defer></script>
</body>

</html>
