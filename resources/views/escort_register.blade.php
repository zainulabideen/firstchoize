@include('partials.navbar')

<body class="hold-transition skin-purple sidebar-mini" id="fullscreen">
    <!-- Site wrapper -->
    <div class="wrapper">
  @include('partials.asidebar')
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
       @if (session('message'))
            <div class="w-form-fail" id="message" style="background: green;font-size: 25px;color: white;">
            <div> {{session('message')}} </div>
          </div>  @endif
            <section class="content-header">
                <h1>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Escort Register</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="head">
                            <h1 class="pull-left" >Escort Register</h1>
                            <!-- <button type="button" class="pull-right button raised primary" data-toggle="modal" data-target="#addNew">Add New</button> -->
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-12">
                        <div class="md-button-box">
                            <div class="btn-group">
                                <button class="md-button" data-toggle="modal" data-target="#addNew" onclick="myfunction('addNew')">Add New</button>
                                <button class="md-button" data-toggle="modal" data-target="#edit" onclick="myfunction('edit')">Edit</button>
                                <button class="md-button">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form name='escort_name' method="POST" id="escort_register">{{ csrf_field() }}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="background">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Escort Name</label><input type='hidden' name='escort_id' id='escort_id'/>
                                            <input type="text" name="escort_name" id='escort_name' class="form-control" placeholder="Escort Name" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                            <input type="text" name="mobile_number" id="mobile_number" class="form-control" placeholder="Mobile Number" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Blood Group</label>
                                            <select class="form-control " id="blood_group"  name="blood_group" style="width: 100%;">
                                                <option value="">Select Blood Group</option>
                                                <option value="A +ve">A +ve</option>
                                                <option value="A -ve">A -ve</option>
                                                <option value="B +ve">B +ve</option>
                                                <option value="B -ve">B -ve</option>
                                                <option value="O +ve">O +ve</option>
                                                <option value="O -ve">O -ve</option>
                                                <option value="AB +ve">AB +ve</option>
                                                <option value="AB -ve">AB -ve</option>
                                            </select>
                                        </div>
                                        <label for="blood_group" class="error"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Company Branch</label>
                                            <select class="form-control " name="branch" id="branch"  style="width: 100%;">
                                                <option value="" >Select Company Branch</option>
                                                @foreach($company as $cmpny)
                                                <option value='{{$cmpny->company_branch_id}}'>{{$cmpny->company_branch_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <label for="branch" class="error"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button type='submit' id='save_btn' class="md-button pull-right">Register</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="table-responsive shadow-z-1">
                                <table class="table" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>
                                            <th>Escort Name</th>
                                            <th>Mobile Number</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($escorts as $key=>$escort)  <tr>
                                            <td>{{$key+1}}. </td>
                                            <td>{{$escort->escort_name}}</td>
                                            <td>{{$escort->escort_mobile_no}}</td>
                                            <td>
                                              <button class="btn btn-warning btn-xs delete" onclick="onclickedit({{$escort->escort_id}})"><i class="fa fa-pencil"></i></button>
                                              <a class="btn btn-danger btn-xs delete" data-toggle="modal"  onclick="onclickdelete({{$escort->escort_id}})"  class="btn btn-info" data-target="#myModal2"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section> <!-- /.content -->
        </div><!-- /.content-wrapper -->


        <div class="modal fade slide-up disable-scroll" id="myModal2" tabindex="-1" role="dialog" display="block" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
              </button>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h3>   <span class="semi-bold">Delete Employee</span></h3>

            </div>
            <div class="panel-body">
          <div class="row">
            <div class="col-sm-10">

        <form class="form-horizontal" method="POST" action="/delete_escort_register" role="form" enctype="multipart/form-data" >{{ csrf_field() }}
       <div class="box-body" >
      <input type='hidden' id='del_escort_id' name='del_escort_id'/>
      Are u sure you want to delete Escort?




         <div class="box-footer">

                  <button type="submit" class="btn btn-success pull-right" id='edit_save_btn'>Delete</button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-right:25%">Cancel</button>
                </div>

       </div>
       </div></form>
            </div>
          </div>
        </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
        <!-- Modal Popup for Add New Starts -->

        <!-- Modal Popup for Add New Ends -->


        <!-- Modal Popup for Edit Office Ends -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.3
            </div>
            <strong>Copyright &copy; 2018-2019 <a href="#">Magic Travels</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
   <!-- jQuery 2.2.0 -->
    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/form-validator.js"></script>
    <script type="text/javascript" src="dist/js/custom-validation.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <script src="dist/js/default.js"></script>
    <script type="text/javascript">
       $('#escort_register_li').addClass('active');
        function myfunction(x)
        {
            ModalPass(x);
        }



    $(function () {
        $(".select2").select2();
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: true
        });
      });
     $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });

      function onclickedit(id)
      {$('#save_btn').html('Update');
      $('#escort_id').val(id);
      var APP_URL="<?php echo env('APP_URL');?>";
      var token="<?php echo Session::get('admin_token')?>";
      $.ajax({
        async: true,
        type : "GET",
        url : APP_URL+"/api/v1/escort/"+id+"?token="+token,
        async:false,
        success : function(response) {
          console.log(response);
          $('#employee_id').val(id);
          $('#escort_name').val(response[0].escort_name);
      $('#mobile_number').val(response[0].escort_mobile_no);
      $('#blood_group').val(response[0].escort_blood_group);
        $('#branch').val(response[0].escort_company_branch_id);

        },
        fail: function(e,b,c) {
         console.log('fail');
        }
      });

      }

      function onclickdelete(id)
      {
        $('#del_escort_id').val(id);

      }

      $('#save_btn').click(function()
        {
          // var employee_id=$('#employee_id').val();
        var save=  $('#save_btn').html();
        var id=  $('#escort_id').val();
        if(save == 'Update'){
          document.escort_name.action ="update_escort_register/"+id;
        }else{
          document.escort_name.action ="escort_register";
        }

       })
    </script>
</body>

</html>
