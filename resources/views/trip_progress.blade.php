@include('partials.navbar')

<body class="hold-transition skin-purple sidebar-mini" id="fullscreen">
    <!-- Site wrapper -->
    <div class="wrapper">
   @include('partials.asidebar')
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
              @if (session('message'))
            <div class="w-form-fail" id="message" style="background: green;font-size: 25px;color: white;">
            <div> {{session('message')}} </div>
          </div>  @endif
            <section class="content-header">
                <h1>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Trip Progress</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="head">
                            <h1 class="pull-left">Trip Progress</h1>
                            <!-- <button type="button" class="pull-right button raised primary" data-toggle="modal" data-target="#addNew">Add New</button> -->
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-12">
                        <div class="md-button-box">
                            <div class="btn-group">
                                <button class="md-button" data-toggle="modal" data-target="#addNew" onclick="myfunction('addNew')">Add New</button>
                                <button class="md-button" data-toggle="modal" data-target="#edit" onclick="myfunction('edit')">Edit</button>
                                <button class="md-button">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="table-responsive shadow-z-1">
                                <table class="table" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>
                                            <th>Trip Code</th>
                                            <th>Date</th>
                                            <th>Type</th>
                                            <th>Shift</th>
                                            <th>Vehicle Number</th>
                                            <th>Escort Allocation</th>
                                            <th>No of employees planned</th>
                                            <th>No of employees travelled</th>
                                            <th>Driver waiting from</th>
                                            <th>Employee Boarding Time</th>
                                            <th>Employee ID</th>
                                            <th>Employee Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                           @foreach($tripprog as $key=>$trip)
                                      <tr>
                                      <td style="text-align:center;">{{$key+1}}</td>
                                           <td style="text-align:center;"> {{$trip->trip_id}}</td>
                                            <td style="text-align:center;">{{date("d-m-Y", strtotime($trip->trip_start_datetime))}}</td>
                                            <td style="text-align:center;">{{$trip->trip_type}}</td>
                                             <td style="text-align:center;">{{$trip->shift_time_from}} {{$trip->shift_time_to}}</td>
                                            <td style="text-align:center;">{{$trip->vehicle_number}}</td>
                                            <td style="text-align:center;">{{$trip->escort_name}}</td>
                                            <td style="text-align:center;">{{$trip->planned_employees}}</td>
                                            <td style="text-align:center;">{{$trip->actual_employees}}</td>
                                            <td style="text-align:center;">{{$trip->waiting_employee_count}}</td>
                                            <td style="text-align:center;">{{date("g:i a", strtotime($trip->trip_start_time))}}</td>
                                             <td style="text-align:center;">{{$trip->employee_ids}}</td>
                                             <td style="text-align:center;">{{$trip->employee_names}}</td>

                                <td style="text-align:center;">

                                                <button class="btn btn-danger btn-xs delete" data-toggle="modal" onclick="onclickStop({{$trip->trip_id}})" data-target="#addNew">stop</button>
                                            </td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="googleMap">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15551.277544019764!2d77.57913564999998!3d12.98340115!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1538394480216" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </section> <!-- /.content -->
        </div><!-- /.content-wrapper -->

        <!-- Modal Popup for Add New Starts -->
            <div class="modal fade" id="addNew" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Stop Trip</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="/stopTrip" method="POST" id="add_office">

                                  {{csrf_field()}}


                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div class="form-group">

                                        <input type="text" name="trip_id" id='trip_id' class="form-control"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type='submit' class="button raised primary pull-right">Stop</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Add New Ends -->

        <!-- Modal Popup for Edit Office Starts -->
            <div class="modal fade" id="edit" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Edit Office</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="#" method="POST" id="edit_office">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label" >Branch Name</p>
                                        <input type="text" name="branchname" class="form-control" placeholder="Branch Name" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label" >Branch ID</p>
                                        <input type="text" name="branchid" class="form-control" placeholder="Branch ID" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Head Office</p>
                                        <select id="one" class="form-control select2" name="headoffice" style="width: 100%;">
                                            <option selected="selected" value="">Select Head Office</option>
                                            <option>Office One</option>
                                            <option>Office Two</option>
                                            <option>Office Three</option>
                                            <option>Office Four</option>
                                            <option>Office Five</option>
                                            <option>Office Six</option>
                                            <option>Office Seven</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Contact Person</p>
                                         <select class="form-control select2" name="contactperson" style="width: 100%;">
                                            <option selected="selected" value="">Select Contact Person</option>
                                            <option>Person One</option>
                                            <option>Person Two</option>
                                            <option>Person Three</option>
                                            <option>Person Four</option>
                                            <option>Person Five</option>
                                            <option>Person Six</option>
                                            <option>Person Seven</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label">Address</p>
                                        <input type="text" name="address" class="form-control" placeholder="Address" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Phone Number</p>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Fax Number</p>
                                        <input type="text" name="" class="form-control" placeholder="Fax Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Pincode</p>
                                        <input type="text" name="pincode" class="form-control" placeholder="Pincode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="button raised primary pull-right">Update</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Edit Office Ends -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.3
            </div>
            <strong>Copyright &copy; 2018-2019 <a href="#">Magic Travels</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
   <!-- jQuery 2.2.0 -->
   <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/form-validator.js"></script>
    <script type="text/javascript" src="dist/js/custom-validation.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <script src="dist/js/default.js"></script>
<script src='https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js'></script>
<script src='https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js'></script>
    <script type="text/javascript">
         $('#trip_progress_li').addClass('active');
        function myfunction(x)
        {
            ModalPass(x);
        }

   function onclickStop(id)
      {
        $('#trip_id').val(id);

      }

    $(document).ready(function() {
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
         {
           extend: 'pdfHtml5',
           title:' Trip Progress',
           orientation: 'landscape',
           pageSize: 'LEGAL',
           exportOptions: {
               columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
           }

         },
         {
           extend: 'excelHtml5',
           title:' Trip Progress',
           orientation: 'landscape',
           pageSize: 'LEGAL',
           exportOptions: {
               columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
           }

         },
         {
           extend: 'print',
           title:' Trip Progress',
           orientation: 'landscape',
           pageSize: 'LEGAL',
           exportOptions: {
               columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
           }

         }

       ]
    } );
} );
    </script>
</body>

</html>
