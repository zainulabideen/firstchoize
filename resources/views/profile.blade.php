<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Magic Travels | Profile</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
    <link rel="stylesheet" href="dist/css/style.css" />
    <link rel="stylesheet" href="plugins/iCheck/all.css">
     <!-- DataTables -->
    <link rel="stylesheet" href="dist/css/data-table.css">
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="dist/css/animate.css" />
</head>

<body class="hold-transition skin-purple sidebar-mini" id="fullscreen">
    <!-- Site wrapper -->
    <div class="wrapper">
@include('partials.asidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Profile</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-widget widget-user">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header bg-black" style="background: url('dist/img/photo1.png') center center;">
                                <h3 class="widget-user-username">Alexander Pierce</h3>
                                <h5 class="widget-user-desc">Admin</h5>
                            </div>
                            <div class="widget-user-image">
                                <img class="img-circle" src="dist/img/user6-128x128.jpg" alt="User Avatar">
                            </div>
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-sm-4 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header">Designation</h5>
                                            <span class="description-text">Admin</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header">Email ID</h5>
                                            <span class="description-text">example@gmail.com</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                        <div class="description-block">
                                            <h5 class="description-header">Mobile No</h5>
                                            <span class="description-text">+91 - 9876543210</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background" style="padding: 10px;">
                            <form method="post" action="#" id="profile">
                                <div class="">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="inputGroupSuccess3"> Name</label>
                                            <input type="text" name="name" class="form-control" placeholder=" Name" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="inputGroupSuccess3"> ID</label>
                                            <input type="text" name="id" class="form-control" placeholder=" ID" />
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="inputGroupSuccess3">Contact Person</label>
                                            <input type="text" name="contact_person" class="form-control" placeholder="Contact Person" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="inputGroupSuccess3">Designation</label>
                                            <input type="text" name="designation" class="form-control" placeholder="Designation" />
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="col-md-6" style="padding-left:0px; padding-right:0px;">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label" for="inputGroupSuccess3">Phone Number</label>
                                                <input type="text" name="phone_number" class="form-control" placeholder="Phone Number" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label" for="inputGroupSuccess3">Mobile Number</label>
                                                <input type="text" name="mobile_number" class="form-control" placeholder="Mobile Number" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="inputGroupSuccess3">Email ID</label>
                                            <input type="text" name="email" class="form-control" placeholder="Email ID" />
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="inputGroupSuccess3">Website</label>
                                            <input type="text" name="website" class="form-control" placeholder="Website" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="inputGroupSuccess3">Skype ID</label>
                                            <input type="text" name="skype" class="form-control" placeholder="Skype ID" />
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="inputGroupSuccess3">Address</label>
                                            <textarea type="text" name="address" class="form-control" placeholder="Address" style="min-height: 80px;"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="inputGroupSuccess3">About Company</label>
                                            <textarea type="text" name="about_company" class="form-control" placeholder="About Company" style="min-height: 80px;"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="inputGroupSuccess3">Profile Picture</label>
                                            <input type="file" name="profile" class="form-control" placeholder="Profile Picture" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="inputGroupSuccess3">Company Logo</label>
                                            <input type="file" name="company" class="form-control" placeholder="Company Logo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="btn-group pull-right">
                                        <button class="md-button" type="button" data-toggle="modal" data-target="#changepassword">Change Password</button>
                                        <button class="md-button-green pull-right">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section> <!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.3
            </div>
            <strong>Copyright &copy; 2018-2019 <a href="#">Magic Travels</a>.</strong> All rights reserved.
        </footer>
        <!-- Modal Popup for Change Password Starts -->
        <div class="modal fade" id="changepassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="#" method="POST">
                            <div class="">
                                <div class="form-group">
                                    <p>Old Password</p>
                                    <input type="password" class="form-control" placeholder="Old Password" />
                                </div>
                                <div class="form-group">
                                    <p>New Password</p>
                                    <input type="password" class="form-control" placeholder="New Password" />
                                </div>
                                <div class="form-group">
                                    <p>New Password</p>
                                    <input type="password" class="form-control" placeholder="New Password" />
                                </div>
                                <div class="form-group">
                                    <button class="md-button pull-right">Change Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Popup for Change Password Ends -->
    </div>
    <!-- ./wrapper -->
    <!-- jQuery 2.2.0 -->
    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <script type="text/javascript" src="dist/js/data-table.js"></script>
    <script type="text/javascript" src="dist/js/form-validator.js"></script>
    <script type="text/javascript" src="dist/js/custom-validation.js"></script>
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/default.js"></script>
    <script type="text/javascript">
        //Date picker
        $('#voucherdate').datepicker({
          autoclose: true
        });

        function myfunction(x)
        {
            ModalPass(x);
        }

    </script>
</body>

</html>
