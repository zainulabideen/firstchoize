@include('partials.navbar')

<body class="hold-transition skin-purple sidebar-mini" id="fullscreen">
    <!-- Site wrapper -->
    <div class="wrapper">
  @include('partials.asidebar')
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          @if (session('message'))
            <div class="w-form-fail" id="message" style="background: green;font-size: 25px;color: white;">
            <div> {{session('message')}} </div>
          </div>  @endif
            <section class="content-header">
                <h1>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Vehicle Register</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="head">
                            <h1 class="pull-left" >Vehicle Register</h1>
                            <!-- <button type="button" class="pull-right button raised primary" data-toggle="modal" data-target="#addNew">Add New</button> -->
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-12">
                        <div class="md-button-box">
                            <div class="btn-group">
                                <button class="md-button" data-toggle="modal" data-target="#addNew" onclick="myfunction('addNew')">Add New</button>
                                <button class="md-button" data-toggle="modal" data-target="#edit" onclick="myfunction('edit')">Edit</button>
                                <button class="md-button">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form name='vehicle_form' method="POST" id="vehicle_register">{{csrf_field()}}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="background">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group"><input type='hidden' id='vehicle_id' name='vehicle_id'>
                                            <label>Owner Name</label>
                                            <input type="text" name="owner_name" id='owner_name' class="form-control" placeholder="Owner Name" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                            <input type="text" name="mobile_number" id='mobile_number' class="form-control" placeholder="Mobile Number" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Email ID</label>
                                            <input type="email" name="email_id" id='email_id' class="form-control" placeholder="Email ID" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input type="text" name="address" id='address' class="form-control" placeholder="Address" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Vehicle Type</label>
                                            <input type="text" name="vehicle_type" id='vehicle_type' class="form-control" placeholder="Vehicle Type" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Vehicle Number</label>
                                            <input type="text" name="vehicle_number" id='vehicle_number'  class="form-control" placeholder="Vehicle Number" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Vehicle Name</label>
                                            <input type="text" name="vehicle_model" id='vehicle_model' class="form-control" placeholder="Vehicle Name" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Manufacturing Year</label>
                                            <input type="text" name="manufacturing_year" id='manufacturing_year' class="form-control" id="datepicker" placeholder="Manufacturing Year" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Total Seats</label>
                                            <input type="text" name="total_seats" id='total_seats' class="form-control" placeholder="Total Seats" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Tax Number</label>
                                            <input type="text" name="tax_number" id='tax_number' class="form-control" placeholder="Tax Number" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Tax Validity</label>
                                            <input type="text" name="tax_validity" id='tax_validity' class="form-control" placeholder="Tax Validity" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Tax Photo</label>
                                            <input type="file" name="tax_photo" id='tax_photo' class="form-control" placeholder="Tax Photo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>RC Number</label>
                                            <input type="text" name="rc_number"id='rc_number' class="form-control" placeholder="RC Number" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>RC Validity</label>
                                            <input type="text" name="rc_validity" id='rc_validity' class="form-control" placeholder="RC Validity" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>RC Photo</label>
                                            <input type="file" name="rc_photo"  id='rc_photo' class="form-control" placeholder="RC Photo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Permit Number</label>
                                            <input type="text" name="permit_number" id='permit_number' class="form-control" placeholder="Permit Number" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group bootstrap-timepicker">
                                            <label>Permit Validity</label>
                                            <input type="text" name="permit_validity" id='permit_validity' class="form-control " placeholder="Permit Validity" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Permit Photo</label>
                                            <input type="file" name="permit_photo" id='permit_photo' class="form-control" placeholder="Permit Photo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Insurance Number</label>
                                            <input type="text" name="insurance_number"  id='insurance_number'class="form-control" placeholder="Insurance Number" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group ">
                                            <label>Insurance Validity</label>
                                            <input type="text" name="insurance_validity"id='insurance_validity' class="form-control " placeholder="Insurance Validity" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Insurance Photo</label>
                                            <input type="file" name="insurance_photo" id='insurance_photo'  class="form-control" placeholder="Insurance Photo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>FC Number</label>
                                            <input type="text" name="fc_number" id='fc_number'class="form-control" placeholder="FC Number" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group bootstrap">
                                            <label>FC Validity</label>
                                            <input type="text" name="fc_validity" id='fc_validity' class="form-control " placeholder="FC Validity" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>FC Photo</label>
                                            <input type="file" name="fc_photo" id='fc_photo'class="form-control" placeholder="FC Photo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button id='save_btn' class="md-button pull-right">Register</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="table-responsive shadow-z-1">
                                <table class="table" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>
                                            <th>Owner Name</th>
                                            <th>Mobile Number</th>
                                            <th>Email ID</th>
                                            <th>Vehicle Number</th>
                                            <th>RC Number</th>
                                            <th>Insurance Number</th>
                                            <th>FC Number</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($vehicle as $key=>$vehi)  <tr>
                                            <td>{{$key+1}}. </td>
                                            <td>{{$vehi->owner_name}}</td>
                                            <td>{{$vehi->vehicle_mobile_no}}</td>
                                            <td>{{$vehi->vehicle_email}}</td>
                                            <td>{{$vehi->vehicle_number}}</td>
                                            <td>{{$vehi->rc_number}}</td>
                                            <td>{{$vehi->insurance_number}}</td>
                                            <td>{{$vehi->fc_number}}</td>
                                            <td>{{$vehi->vehicle_status}}</td>
                        <td>                      <button class="btn btn-warning btn-xs delete" onclick="onclickedit({{$vehi->vehicle_id}})"><i class="fa fa-pencil"></i></button>
                                            <a class="btn btn-danger btn-xs delete" data-toggle="modal"  onclick="onclickdelete({{$vehi->vehicle_id}})"  class="btn btn-info" data-target="#myModal2"><i class="fa fa-trash"></i></a>
                                          </td></tr>
                                        @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section> <!-- /.content -->
        </div><!-- /.content-wrapper -->
        <div class="modal fade slide-up disable-scroll" id="myModal2" tabindex="-1" role="dialog" display="block" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
              </button>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h3>   <span class="semi-bold">Delete Vehicle</span></h3>

            </div>
            <div class="panel-body">
          <div class="row">
            <div class="col-sm-10">

        <form class="form-horizontal" method="POST" action="/delete_vehicle_register" role="form" enctype="multipart/form-data" >{{ csrf_field() }}
       <div class="box-body" >
      <input type='hidden' id='del_vehicle_id' name='del_vehicle_id'/>
      Are u sure you want to delete vehicle?




         <div class="box-footer">

                  <button type="submit" id='save_btn' class="btn btn-success pull-right" id='edit_save_btn'>Delete</button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-right:25%">Cancel</button>
                </div>

       </div>
       </div></form>
            </div>
          </div>
        </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>



        <!-- Modal Popup for Add New Ends -->

        <!-- Modal Popup for Edit Office Starts -->

        <!-- Modal Popup for Edit Office Ends -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.3
            </div>
            <strong>Copyright &copy; 2018-2019 <a href="#">Magic Travels</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
   <!-- jQuery 2.2.0 -->
    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- bootstrap datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/form-validator.js"></script>
    <script type="text/javascript" src="dist/js/custom-validation.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <script src="dist/js/default.js"></script>
    <script type="text/javascript">
        function myfunction(x)
        {
            ModalPass(x);
        }

$('#vehicle_register_li').addClass('active');

    $(function () {
         //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: " yyyy",
      viewMode: "years",
    minViewMode: "years"

    });
        $(".select2").select2();
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: true
        });
      });
     $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });

      function onclickedit(id)
      {$('#save_btn').html('Update');
      var APP_URL="<?php echo env('APP_URL');?>";
      var token="<?php echo Session::get('admin_token')?>";
      $.ajax({
        async: true,
        type : "GET",
        url : APP_URL+"/api/v1/vehicle/"+id+"?token="+token,
        async:false,
        success : function(response) {
          $('#vehicle_id').val(id);
    $('#vehicle_name').val(response[0].vehicle_name);
  $('#owner_name').val(response[0].owner_name);
  $('#mobile_number').val(response[0].vehicle_mobile_no);
  $('#email_id').val(response[0].vehicle_email);
  $('#address').val(response[0].vehicle_address);
  $('#vehicle_type').val(response[0].vehicle_type);
  $('#vehicle_number').val(response[0].vehicle_number);
  $('#vehicle_model').val(response[0].vehicle_model);
  $('#manufacturing_year').val(response[0].manufacturing_year);
  $('#total_seats').val(response[0].total_seats);
  $('#tax_number').val(response[0].tax_number);
  $('#tax_validity').val(response[0].tax_validity);
  $('#rc_number').val(response[0].rc_number);
$('#rc_validity').val(response[0].rc_validity);
$('#permit_number').val(response[0].permit_number);
$('#permit_validity').val(response[0].permit_validity);
$('#insurance_number').val(response[0].insurance_number);
$('#insurance_validity').val(response[0].insurance_validity);
$('#fc_number').val(response[0].fc_number);
$('#fc_validity').val(response[0].fc_validity);


          console.log(values);
        },
        fail: function(e,b,c) {
         console.log('fail');
        }
      });

      }

      function onclickdelete(id)
      {
        $('#del_vehicle_id').val(id);

      }

      $('#save_btn').click(function()
        {

        var save=  $('#save_btn').html();
        var id=  $('#vehicle_id').val();
        if(save == 'Update'){
          document.vehicle_form.action ="update_vehicle/"+id;
        }else{
          document.vehicle_form.action ="vehicle_register";
        }

       })
    </script>
</body>

</html>
