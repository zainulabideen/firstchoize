@include('partials.navbar')

<body class="hold-transition skin-purple sidebar-mini" id="fullscreen">
    <!-- Site wrapper -->
    <div class="wrapper">
    @include('partials.asidebar')
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
                 @if (session('message'))
            <div class="w-form-fail" id="message" style="background: green;font-size: 25px;color: white;">
            <div> {{session('message')}} </div>
          </div>  @endif
            <section class="content-header">
                <h1>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Driver Register</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="head">
                            <h1 class="pull-left" >Driver Register</h1>
                            <!-- <button type="button" class="pull-right button raised primary" data-toggle="modal" data-target="#addNew">Add New</button> -->
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-12">
                        <div class="md-button-box">
                            <div class="btn-group">
                                <button class="md-button" data-toggle="modal" data-target="#addNew" onclick="myfunction('addNew')">Add New</button>
                                <button class="md-button" data-toggle="modal" data-target="#edit" onclick="myfunction('edit')">Edit</button>
                                <button class="md-button">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form name='driver_form' method="POST" id="driver_register">{{csrf_field()}}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="background">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group"><input type = hidden id='driver_id' name='driver_id'>
                                            <label>Driver Name</label>
                                            <input type="text" name="driver_name"  id="driver_name" class="form-control" placeholder="Driver Name" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                            <input type="text" name="mobile_number"  id="mobile_number" class="form-control" placeholder="Mobile Number" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Email ID</label>
                                            <input type="email" name="email_id" id="email_id" class="form-control" placeholder="Email ID" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input type="text" name="address"   id="address"  class="form-control" placeholder="Address" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Driver Photo</label>
                                            <input type="file" name="driver_photo" id="driver_photo" class="form-control" placeholder="Driver Photo" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Blood Group</label>
                                            <select class="form-control " name="blood_group" id="blood_group" style="width: 100%;">
                                                <option value="" >Select Blood Group</option>
                                                <option value="A +ve">A +ve</option>
                                                <option value="A -ve">A -ve</option>
                                                <option value="B +ve">B +ve</option>
                                                <option value="B -ve">B -ve</option>
                                                <option value="O +ve">O +ve</option>
                                                <option value="O -ve">O -ve</option>
                                                <option value="AB +ve">AB +ve</option>
                                                <option value="AB -ve">AB -ve</option>
                                            </select>
                                        </div>
                                        <label for="blood_group" class="error"></label>
                                    </div>
                                </div>
                                <div class="row">

                                  <div class="col-sm-4 col-xs-12">
                                      <div class="form-group">
                                          <label>Driver Code</label>
                                          <input type="text" required name="driver_code"  id="driver_code" class="form-control" placeholder="Driver Name" />
                                      </div>
                                  </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Emergency Contact Number 1</label>
                                            <input type="text" name="emr_contact_number1" id="emr_contact_number1" class="form-control" placeholder="Emergency Contact Number 1" />
                                        </div>
                                    </div>

                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Emergency Contact Address</label>
                                            <input type="text" name="emr_contact_address"  id="emr_contact_address" class="form-control" placeholder="Emergency Contact Address" />
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>DL Number</label>
                                            <input type="text" name="dl_number"  id="dl_number" class="form-control" placeholder="DL Number" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>DL Validity</label>
                                            <input type="text" name="dl_validity"  id="dl_validity" class="form-control" placeholder="DL Validity" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>DL Photo</label>
                                            <input type="file" name="dl_photo" class="form-control" placeholder="DL Photo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-4 col-xs-12">
                                      <div class="form-group">
                                          <label>Emergency Contact Name</label>
                                          <input type="text" name="emr_contact_name"  id="emr_contact_name" class="form-control" placeholder="Emergency Contact Name" />
                                      </div>
                                  </div>

                                    <div class="col-xs-12">
                                        <button id='save_btn' class="md-button pull-right">Register</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="googleMap">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15551.277544019764!2d77.57913564999998!3d12.98340115!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1538394480216" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="table-responsive shadow-z-1">
                                <table class="table" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>
                                            <th>Driver Name</th>
                                            <th>Mobile Number</th>
                                            <th>Email ID</th>
                                            <th>DL Number</th>
                                            <!-- <th>Password</th> -->
                                            <!-- <th>Status</th> -->
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($driver as $key=>$drive)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$drive->driver_name}}</td>
                                            <td>{{$drive->driver_mobile_no}}</td>
                                            <td>{{$drive->driver_email}}</td>
                                            <td>{{$drive->dl_number}}</td>

                                            <!-- <td>Active</td> -->
                                            <td>
                                              <button class="btn btn-warning btn-xs delete" onclick="onclickedit({{$drive->driver_id}})"><i class="fa fa-pencil"></i></button>
                                              <!-- <a class="btn btn-danger btn-xs delete" data-toggle="modal"  onclick="onclickdelete({{$drive->driver_id}})"  class="btn btn-info" data-target="#myModal2"><i class="fa fa-trash"></i></a> -->
                                            </td>
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section> <!-- /.content -->
        </div><!-- /.content-wrapper -->

        <div class="modal fade slide-up disable-scroll" id="myModal2" tabindex="-1" role="dialog" display="block" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
              </button>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h3>   <span class="semi-bold">Delete Driver</span></h3>

            </div>
            <div class="panel-body">
          <div class="row">
            <div class="col-sm-10">

        <form class="form-horizontal" method="POST" action="/deletedriver" role="form" enctype="multipart/form-data" >{{ csrf_field() }}
       <div class="box-body" >
      <input type='hidden' id='del_driver_id' name='del_driver_id'/>
      Are u sure you want to delete Driver?




         <div class="box-footer">

                  <button type="submit" id='save_btn' class="btn btn-success pull-right" id='edit_save_btn'>Delete</button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-right:25%">Cancel</button>
                </div>

       </div>
       </div></form>
            </div>
          </div>
        </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.3
            </div>
            <strong>Copyright &copy; 2018-2019 <a href="#">Magic Travels</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
   <!-- jQuery 2.2.0 -->
    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/form-validator.js"></script>
    <script type="text/javascript" src="dist/js/custom-validation.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <script src="dist/js/default.js"></script>
    <script type="text/javascript">
         $('#driver_register_li').addClass('active');
        function myfunction(x)
        {
            ModalPass(x);
        }



    $(function () {
        $(".select2").select2();
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: true
        });
      });
     $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
      function onclickdelete(id)
      {
        $('#del_driver_id').val(id);

      }
      function onclickedit(id)
      {$('#save_btn').html('Update');
      var APP_URL="<?php echo env('APP_URL');?>";
      var token="<?php echo Session::get('admin_token')?>";
      $.ajax({
        async: true,
        type : "GET",
        url : APP_URL+"/api/v1/driver/"+id+"?token="+token,
        async:false,
        success : function(response) {

          var values=response['driver_details'];
          console.log(values);
          $('#driver_id').val(id);
      $('#driver_name').val(values.driver_name);
  $('#email_id').val(values.driver_email);
  $('#driver_code').val(values.driver_code);
  $('#mobile_number').val(values.driver_mobile_no);
  $('#emr_contact_number1').val(values.emergency_contact_no);
  $('#emr_contact_name').val(values.emergency_contact_name);
  $('#blood_group').val(values.driver_blood_group);
  $('#address').val(values.driver_address);
  $('#emr_contact_address').val(values.emergency_address);
  $('#dl_number').val(values.dl_number);
  // $('#dl_photo').val(values);
  $('#dl_validity').val(values.dl_validity);
},
fail: function(e,b,c) {
 console.log('fail');
}
});

}

      $('#save_btn').click(function()
        {
          var driver_id=$('#driver_id').val();
        var save=  $('#save_btn').html();
        var id=  $('#driver_id').val();
        // alert(save);
        // alert(id);
        if(save == 'Update'){
          // alert('in');
          document.driver_form.action ="updatedriver/"+id;
        }else{
          document.driver_form.action ="driver_register";
        }

       })
    </script>
</body>

</html>
