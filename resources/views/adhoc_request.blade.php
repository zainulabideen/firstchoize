
  @include('partials.navbar')
<body class="hold-transition skin-purple sidebar-mini" id="fullscreen">
    <!-- Site wrapper -->
    <div class="wrapper">
      @include('partials.asidebar')
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Adhoc Request</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="head">
                            <h1 class="pull-left" >Adhoc Request</h1>
                            <!-- <button type="button" class="pull-right button raised primary" data-toggle="modal" data-target="#addNew">Add New</button> -->
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-12">
                        <div class="md-button-box">
                            <div class="btn-group">
                                <button class="md-button" data-toggle="modal" data-target="#addNew" onclick="myfunction('addNew')">Add New</button>
                                <button class="md-button" data-toggle="modal" data-target="#edit" onclick="myfunction('edit')">Edit</button>
                                <button class="md-button">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="#" method="POST" id="adhoc_request">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="background">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Company Branch</label>
                                            <select class="form-control select2" name="company_branch" style="width: 100%;">
                                                <option value="" selected="selected">Select Company Branch</option>
                                                <option value="Branch 1">Branch 1</option>
                                                <option value="Branch 2">Branch 2</option>
                                            </select>
                                        </div>
                                        <label for="company_branch" class="error"></label>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Booked By</label>
                                            <select class="form-control select2" name="booked_by" style="width: 100%;">
                                                <option value="" selected="selected">Select Booked By</option>
                                                <option value="17571 - Sukalyan Naskar">17571 - Sukalyan Naskar</option>
                                                <option value="17571 - Sukalyan Naskar">B17571 - Sukalyan Naskar</option>
                                            </select>
                                        </div>
                                        <label for="booked_by" class="error"></label>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Guest Name</label>
                                            <input type="text" name="guest_name" class="form-control" placeholder="Guest Name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Guest Cell Number </label>
                                            <input type="text" name="guest_cell_number" class="form-control" placeholder="Guest Cell Number" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Report At</label>
                                            <input type="text" name="report_at" class="form-control" placeholder="Report At" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Trip To</label>
                                            <input type="text" name="trip_to" class="form-control" placeholder="Trip To" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Date </label>
                                            <input type="text" name="date" id="datepicker" class="form-control" placeholder="Date" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group bootstrap-timepicker">
                                            <label>Time</label>
                                            <input type="text" name="time" class="form-control timepicker" placeholder="Time" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Driver Name</label>
                                            <input type="text" name="driver_name" class="form-control" placeholder="Driver Name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Driver Cell Number</label>
                                            <input type="text" name="driver_cell_number"  class="form-control" placeholder="Driver Cell Number" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Car Number</label>
                                            <input type="text" name="car_number" class="form-control" placeholder="Car Number" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Closed By</label>
                                            <input type="text" name="closed_by" class="form-control" placeholder="Closed By" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Other City</label>
                                            <select class="form-control select2" name="other_city" style="width: 100%;">
                                                <option value="" selected="selected">Select Other City</option>
                                                <option value="No">No</option>
                                                <option value="Yes">Yes</option>
                                            </select>
                                        </div>
                                        <label for="other_city" class="error"></label>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Closing Kms</label>
                                            <input type="text" name="closing_kms" class="form-control" placeholder="Closing Kms" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Closing Date</label>
                                            <input type="text" name="closing_date" class="form-control" id="datepicker1" placeholder="Closing Date" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group bootstrap-timepicker">
                                            <label>Closing Time</label>
                                            <input type="text" name="closing_time" class="form-control timepicker" placeholder="Closing Time" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Opening Kms</label>
                                            <input type="text" name="opening_kms" class="form-control" placeholder="Opening Kms" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Opening Date</label>
                                            <input type="text" name="opening_date" class="form-control" id="datepicker2" placeholder="Opening Date" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group bootstrap-timepicker">
                                            <label>Opening Time</label>
                                            <input type="text" name="opening_time" class="form-control timepicker" placeholder="Opening Time" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Total Kms</label>
                                            <input type="text" name="total_kms" class="form-control" placeholder="Total Kms" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Toll & Parking</label>
                                            <input type="text" name="toll_parking" class="form-control" placeholder="Toll & Parking" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group bootstrap-timepicker">
                                            <label>From Office</label>
                                            <input type="text" name="from_office" class="form-control" placeholder="From Office" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Punctuality</label>
                                            <select class="form-control select2" name="panctuality" style="width: 100%;">
                                                <option value="" selected="selected">Select Punctuality</option>
                                                <option value="0">Excellent</option>
                                                <option value="1">Good</option>
                                                <option value="2">Satisfactory</option>
                                            </select>
                                        </div>
                                        <label for="panctuality" class="error"></label>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Courtesy</label>
                                            <select class="form-control select2" name="courtesy" style="width: 100%;">
                                                <option value="" selected="selected">Select Courtesy</option>
                                                <option value="0">Excellent</option>
                                                <option value="1">Good</option>
                                                <option value="2">Satisfactory</option>
                                            </select>
                                        </div>
                                        <label for="courtesy" class="error"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group bootstrap-timepicker">
                                            <label>Condition</label>
                                            <select class="form-control select2" name="condition" style="width: 100%;">
                                                <option value="" selected="selected">Select Condition</option>
                                                <option value="0">Excellent</option>
                                                <option value="1">Good</option>
                                                <option value="2">Satisfactory</option>
                                            </select>
                                        </div>
                                        <label for="condition" class="error"></label>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Reason For Complaints</label>
                                            <input type="text" name="reson_for_complaints" class="form-control" placeholder="Reason For Complaints" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button class="md-button pull-right">Request</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="table-responsive shadow-z-1">
                                <table class="table" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>
                                            <th>Company Name</th>
                                            <th>Company Address</th>
                                            <th>Booked By</th>
                                            <th>Guest Name</th>
                                            <th>Guest Mobile</th>
                                            <th>Trip To</th>
                                            <th>Report At</th>
                                            <th>Report Time</th>
                                            <th>Driver Name</th>
                                            <th>Driver Mobile</th>
                                            <th>Car Number</th>
                                            <th>Prepared By</th>
                                            <th>Closed By</th>
                                            <th>Other City</th>
                                            <th>Opening Kms</th>
                                            <th>Closing Kms</th>
                                            <th>Opening Date</th>
                                            <th>Closing Date</th>
                                            <th>Opening Time</th>
                                            <th>Closing Time</th>
                                            <th>Total Kms</th>
                                            <th>Total Hrs</th>
                                            <th>Toll & Parking</th>
                                            <th>From Office</th>
                                            <th>Punctuality</th>
                                            <th>Courtesy</th>
                                            <th>Condition</th>
                                            <th>Reson for Compaint</th>
                                            <th>Print</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1. </td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>11</td>
                                            <td>
                                                <button class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section> <!-- /.content -->
        </div><!-- /.content-wrapper -->

        <!-- Modal Popup for Add New Starts -->
            <div class="modal fade" id="addNew" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Add Office</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="#" method="POST" id="add_office">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label" >Branch Name <span class="red">*</span> </p>
                                        <input type="text" name="branchname" class="form-control" placeholder="Branch Name" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label" >Branch ID <span class="red">*</span></p>
                                        <input type="text" name="branchid" class="form-control" placeholder="Branch ID" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Head Office <span class="red">*</span></p>
                                        <select class="form-control select2" name="headoffice" style="width: 100%;">
                                            <option selected="selected" value="">Select Head Office</option>
                                            <option>Office One</option>
                                            <option>Office Two</option>
                                            <option>Office Three</option>
                                            <option>Office Four</option>
                                            <option>Office Five</option>
                                            <option>Office Six</option>
                                            <option>Office Seven</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Contact Person <span class="red">*</span></p>
                                        <select class="form-control select2" name="contactperson" style="width: 100%;">
                                            <option selected="selected" value="">Select Contact Person</option>
                                            <option>Person One</option>
                                            <option>Person Two</option>
                                            <option>Person Three</option>
                                            <option>Person Four</option>
                                            <option>Person Five</option>
                                            <option>Person Six</option>
                                            <option>Person Seven</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label">Address <span class="red">*</span></p>
                                        <input type="text" name="address" class="form-control" placeholder="Address" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Phone Number <span class="red">*</span></p>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Fax Number</p>
                                        <input type="text" name="fax" class="form-control" placeholder="Fax Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Pincode <span class="red">*</span></p>
                                        <input type="text" name="pincode" class="form-control" placeholder="Pincode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="button raised primary pull-right">Add</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Add New Ends -->

        <!-- Modal Popup for Edit Office Starts -->
            <div class="modal fade" id="edit" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Edit Office</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="#" method="POST" id="edit_office">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label" >Branch Name</p>
                                        <input type="text" name="branchname" class="form-control" placeholder="Branch Name" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label" >Branch ID</p>
                                        <input type="text" name="branchid" class="form-control" placeholder="Branch ID" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Head Office</p>
                                        <select id="one" class="form-control select2" name="headoffice" style="width: 100%;">
                                            <option selected="selected" value="">Select Head Office</option>
                                            <option>Office One</option>
                                            <option>Office Two</option>
                                            <option>Office Three</option>
                                            <option>Office Four</option>
                                            <option>Office Five</option>
                                            <option>Office Six</option>
                                            <option>Office Seven</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Contact Person</p>
                                         <select class="form-control select2" name="contactperson" style="width: 100%;">
                                            <option selected="selected" value="">Select Contact Person</option>
                                            <option>Person One</option>
                                            <option>Person Two</option>
                                            <option>Person Three</option>
                                            <option>Person Four</option>
                                            <option>Person Five</option>
                                            <option>Person Six</option>
                                            <option>Person Seven</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label">Address</p>
                                        <input type="text" name="address" class="form-control" placeholder="Address" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Phone Number</p>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Fax Number</p>
                                        <input type="text" name="" class="form-control" placeholder="Fax Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Pincode</p>
                                        <input type="text" name="pincode" class="form-control" placeholder="Pincode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="button raised primary pull-right">Update</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Edit Office Ends -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.3
            </div>
            <strong>Copyright &copy; 2018-2019 <a href="#">Magic Travels</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
   <!-- jQuery 2.2.0 -->
    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- bootstrap datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/form-validator.js"></script>
    <script type="text/javascript" src="dist/js/custom-validation.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <script src="dist/js/default.js"></script>
    <script type="text/javascript">
        function myfunction(x)
        {
            ModalPass(x);
        }



    $(function () {

        $('#adhoc_request_li').addClass('active');
         //Date picker
    $('#datepicker, #datepicker1, #datepicker2').datepicker({
      autoclose: true
    });
        $(".select2").select2();
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: true
        });
      });
     $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });

    </script>
</body>

</html>
