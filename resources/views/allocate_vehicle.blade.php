@include('partials.navbar')
<body class="hold-transition skin-purple sidebar-mini" id="fullscreen">
    <!-- Site wrapper -->
    <div class="wrapper">
    @include('partials.asidebar')
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
              @if (session('message'))
            <div class="w-form-fail" id="message" style="background: green;font-size: 25px;color: white;">
            <div> {{session('message')}} </div>
          </div>  @endif
            <section class="content-header">
                <h1>
                    @if($alloc_value=='N')
                    <?php $date= date("y-m-d");?>
                    <form name="duplicate" method="POST" action="/duplicate_trip">{{csrf_field()}}
                      <input type = 'date' required placeholder="Select Date" name = "given_date">
                      <input type = "hidden" name = "type" value = '{{$type}}'>
                      <button type = 'submit' style = "background:green;color:white;">Allocate</button>
                    </form>
                    <!-- <a href='duplicate_trip/{{$date}}/{{$type}}' style="background: #ff6a00;color: white;padding: 8px;">Allocate  Regular {{$type}} trip</a> -->
                    @endif
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Allocate {{$type}} Vehicle</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <form action="/allocate_vehicle" method="POST" id="allocate_vehicle">{{csrf_field()}}
                <div class="row">
                    <div class="col-xs-12">
                        <div class="head">
                            <h1 class="pull-left">Allocate Vehicle {{$type}}</h1>
                            <!-- <button type="button" class="pull-right button raised primary" data-toggle="modal" data-target="#addNew">Add New</button> -->
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-12">
                        <div class="md-button-box">
                            <div class="btn-group">
                                <button class="md-button" data-toggle="modal" data-target="#addNew" onclick="myfunction('addNew')">Add New</button>
                                <button class="md-button" data-toggle="modal" data-target="#edit" onclick="myfunction('edit')">Edit</button>
                                <button class="md-button">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="/allocate_vehicle" method="POST" id="allocate_vehicle">{{csrf_field()}}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="background">
                                <div class="row">
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            <label>Vehicle Number</label>
                                            <select class="form-control select2" name="vehicle_id" id='vehicle_select_id' style="width: 100%;">
                                              <option value="" selected="selected">Select Vehicle Number</option>
                                              @foreach($vehicles as $vehicle)
                                              <option value='{{$vehicle->vehicle_id}}'>{{$vehicle->vehicle_number}}</option>
                                              @endforeach

                                            </select>
                                        </div>
                                        <label for="vehicle_number" class="error"></label>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            <label>Escort</label>
                                            <select class="form-control select2" name="escort_id" style="width: 100%;">
                                              <option value="" selected="selected">Select Escort</option>
                                              @foreach($escorts as $escort)
                                              <option value='{{$escort->escort_id}}-{{$escort->escort_name}}'>{{$escort->escort_name}}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                        <label for="escort" class="error"></label>
                                    </div>

                                      <div class="col-sm-3 col-xs-12" style="    margin-top: 29px;">
                                        <div class="form-group">

                                        @if($type=='login')
                                        <input type='radio' checked class='radio_class' name='trip_type' id='login' value='login'>
                                        @else
                                        <input type='radio' class='radio_class' name='trip_type' id='login' value='login'>
                                        @endif
                                        <!-- <input type='radio' name='type' id='logout' value='logout'> -->

                                         <a id='login_href' href='/allocate_vehiclelogin'></a>
                                         <label id='login_label' for='login'>Login</label></a>
                                        <!-- <label id='login_label' for='logout'>logout</label> -->

                                        </div>
                                        <label for="escort" class="error"></label>
                                    </div>

                                          <div class="col-sm-3 col-xs-12" style="    margin-top: 29px;">
                                        <div class="form-group">

                                        <!-- <input type='radio' name='type' id='login' value='login'> -->
                                        @if($type=='logout')<input type='radio' class='radio_class' checked name='trip_type' id='logout' value='logout'>

                                        @else
                                        <input type='radio' class='radio_class' name='trip_type' id='logout' value='logout'>
                                        @endif

                                      <a id='logout_href' href='/allocate_vehiclelogout'></a>  <label id='logout_label'  for='logout'>logout</label>

                                        </div>
                                        <label for="escort" class="error"></label>
                                    </div>
                                </div>
                                <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label id='total_label'>Total seats in selected vehicle is

                                            </label>
                                            <br><br>
                                         <label  id='login_l'>Employees assigned for login trip</label><br><br>
                                         <label id='logout_l'>Employees assigned for logout trip </label><br>
                                        </div>
                                        <label for="escort" class="error"></label>
                                    </div>
                                    <div class="col-xs-12">
                                        <button  type='submit' class="md-button pull-right">Allocate Vehicle</button>
<a type='submit' id='submit' class="md-button pull-right">View Route</a>
                                    </div>
                                </div>
                                 <div class="row">
               <input type='hidden' name='first_pickup_address' id='first_pickup_address'/>
                    <input type="hidden" name="last_drop_address" id='last_drop_address'>
                    <input type="hidden" name="trip_start_time" id='trip_start_time'>
                    <input type="hidden" name="distance" id='distance'>


                <!-- </form>  -->

                                 </div>
                            </div>
                        </div>
                    </div>

                <div class="row">
                    <div class="col-xs-12">
                         <div id="map" style="width:100%;height:300px;"></div>
                           <div id="directions-panel"></div>
                       <!--  <div class="googleMap">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15551.277544019764!2d77.57913564999998!3d12.98340115!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1538394480216" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div> -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="box-header with-border">
                              <i class="ion ion-ios-list-outline"></i>
                              <h3 class="box-title">Employee List For Vehicle Allocation</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="table-responsive shadow-z-1">
                                <table class="table" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>

                                            <th>Employee ID</th>
                                            <th>Employee Name</th>
                                            <th>Employee Shift</th>
                                            <th>Route</th>
                                            <th>distance</th>
                                            <th>Employee Address</th>
                                            <th>Company Address</th>

                                            @if($type=='login')
                                            <th>Pickup Time</th>
                                            @else
                                            <th>Drop Time</th>
                                            @endif

                                            <th>Assign</th>
                                        </tr>
                                    </thead>
                                    <tbody>@if(!empty($not_alloc_employee_list))
                      @foreach($not_alloc_employee_list as $key=>$data)
                                        <tr>
                                            <td>{{$key+1}}. </td>

                                            <td>{{$data->employee_code}}</td>
                                            <td>{{$data->employee_name}}</td>
                                            <td>{{$data->shift_name}}</td>
                                            <td>{{$data->route_area}}</td>
                                            <td>{{$data->distance}}</td>

                                            <td>{{$data->employee_pickup_address}}</td>

                                            <td>{{$data->company_address}}</td>

 @if($type=='login')
                                            <td>{{date("g:i a ", strtotime($data->employee_pickup_time))}}</td>
                                            @else
                                            <td>{{date("g:i a", strtotime($data->shift_time_to))}}</td>
                                            @endif
                                            <td>
                                                <input type="checkbox" name="employee_id[]" value={{$data->employee_id}} />
                                            </td>
                                        </tr>




                                      @endforeach
                                      @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="box-header with-border">
                              <i class="ion ion-ios-list-outline"></i>
                              <h3 class="box-title">Allocated Vehicle Report</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="background">
                            <div class="table-responsive shadow-z-1">
                                <table class="table" id="example2">
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>
                                            <!-- <th>Client Name</th> -->
                                            <th>Employee ID</th>
                                            <th>Employee Name</th>
                                            <th>Employee Shift</th>
                                            <th>Route</th>
                                            <th>Distance</th>
                                            <th>Address</th>
                                            <th>Company Address</th>
                                            <th> Vehicle Number</th>
                                            <!-- <th>Vehicle Number</th> -->
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @if(!empty($alloc_employee_list))
                                         @foreach($alloc_employee_list  as $key=>$data)

                                        <tr>
                                             <td>{{$key+1}}. </td>

                                            <td>{{$data->employee_code}}</td>
                                             <td>{{$data->employee_name}}</td>
                                            <td>{{$data->shift_name}}</td>
                                            <td>{{$data->route_area}}</td>
                                            <td>{{$data->distance}}</td>

                                            <td>{{$data->employee_pickup_address}}</td>

                                            <td>{{$data->company_address}}</td>
                                            <td>{{$data->vehicle_number}}</td>
                                            <td>
                                               <a class="btn btn-danger btn-xs delete" data-toggle="modal"  onclick="onclickdelete({{$data->employee_vehicle_meger_id}})"  class="btn btn-info" data-target="#myModal2"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>  </form>
            </section> <!-- /.content -->
        </div><!-- /.content-wrapper -->


  <div class="modal fade slide-up disable-scroll" id="myModal2" tabindex="-1" role="dialog" display="block" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
              </button>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h3>   <span class="semi-bold">Delete Employee</span></h3>

            </div>
            <div class="panel-body">
          <div class="row">
            <div class="col-sm-10">

        <form class="form-horizontal" method="POST" action="/delete_allocate_vehicle" role="form" enctype="multipart/form-data" >{{ csrf_field() }}
       <div class="box-body" >
      <input type='hidden' id='employee_vehicle_meger_id' name='employee_vehicle_meger_id'/>
      Are u sure you want to delete Allocated Employee?




         <div class="box-footer">

                  <button type="submit" id='save_btn' class="btn btn-success pull-right" id='edit_save_btn'>Delete</button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-right:25%">Cancel</button>
                </div>

       </div>
       </div></form>
            </div>
          </div>
        </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
        <!-- Modal Popup for Add New Starts -->


        <!-- Modal Popup for Add New Ends -->

        <!-- Modal Popup for Edit Office Starts -->

        <!-- Modal Popup for Edit Office Ends -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.3
            </div>
            <strong>Copyright &copy; 2018-2019 <a href="#">Magic Travels</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
   <!-- jQuery 2.2.0 -->
    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/form-validator.js"></script>
    <script type="text/javascript" src="dist/js/custom-validation.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <script src="dist/js/default.js"></script>
    <script type="text/javascript">
         $('#allocate_vehicle_li').addClass('active');
        function myfunction(x)
        {
            ModalPass(x);
        }
 $('input[name="trip_type"]').change(function()
 {
var type=document.querySelector('input[name="trip_type"]:checked').value;

if(type=='login'){
location.href = "allocate_vehiclelogin";
}
else if(type=='logout')
{
location.href = "allocate_vehiclelogout";
}
});



    $(function () {
        $(".select2").select2();
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: true
        });
      });
     $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable(
        // {
        //   "paging": true,
        //   "lengthChange": false,
        //   "searching": false,
        //   "ordering": true,
        //   "info": true,
        //   "autoWidth": false
        // }
        );
      });
      var largestindex = 0;
 $('#vehicle_select_id').change(function()
      {
        var id=$('#vehicle_select_id').val();

      var APP_URL="<?php echo env('APP_URL');?>";
        var token="<?php echo Session::get('admin_token')?>";
        $.ajax({
            async: true,
            type : "GET",
            url : APP_URL+"/api/v1/countvehicleseat/"+id+"?token="+token,
            async:false,
            success : function(response) {

          console.log(response);
          $('#total_label').html('Total seats in selected vehicle is  :'+response.vehicletotalseat);
          $('#login_l').html('Employees assigned for login trip is :'+response.login_seats);
         $('#logout_l').html('Employees assigned for logout trip is :'+response.logout_seats);



            },
            fail: function(e,b,c) {
             console.log('fail');
            }
        });

      });

      function onclickdelete(id)
      {
        $('#employee_vehicle_meger_id').val(id);

      }
var valuesformap = new Array();
var company_address = '';
var trip_start_time = new Array();
var trip_distance = new Array();
   $("input[name='employee_id[]']").click(function(){


      var values = new Array();
      var distances = new Array();
      var pickuptimes=new Array();
       $.each($("input[name='employee_id[]']:checked").closest("tr").find('td:eq(6)'),
              function () {
                   values.push($(this).text());


              });
              $.each($("input[name='employee_id[]']:checked").closest("tr").find('td:eq(7)'),
                     function () {
                        company_address =  $(this).text();


                     });

              valuesformap = values;
              $.each($("input[name='employee_id[]']:checked").closest("tr").find('td:eq(5)'),
                     function () {
                          distances.push($(this).text());


                     });
                     trip_distance=distances;

                $.each($("input[name='employee_id[]']:checked").closest("tr").find('td:eq(8)'),
              function () {
                   pickuptimes.push($(this).text());


              });trip_start_time=pickuptimes;


var largest= 0;

for (i=0; i<=largest;i++){
    if (distances[i]>largest) {
         largest=distances[i];
         largestindex = i;
    }
}
console.log(largest);
var type=document.querySelector('input[name="trip_type"]:checked').value;
if(type=='login'){
    $('#first_pickup_address').val(valuesformap[largestindex]);
    $('#trip_start_time').val(trip_start_time[largestindex]);
    $('#distance').val(trip_distance[largestindex]);
$('#last_drop_address').val(company_address);
}else if(type=='logout'){
$('#first_pickup_address').val(company_address);
$('#last_drop_address').val(valuesformap[largestindex]);
$('#trip_start_time').val(trip_start_time[largestindex]);
$('#distance').val(trip_distance[largestindex]);
       // alert("val---" + values.join (", "));
     }
 }
);

$(document).ready(function() {
    // $("input[name='employee_id[]']").click(myfunc);
});

    </script>
    <script>
      function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 1,
          center: {lat: 12.9716, lng: -77.5946}
        });
        directionsDisplay.setMap(map);

        document.getElementById('submit').addEventListener('click', function() {
          calculateAndDisplayRoute(directionsService, directionsDisplay);
        });
      }
var geocoder = new google.maps.Geocoder();
      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var waypts = [];

        for (var i = 0; i < valuesformap.length; i++) {


            waypts.push({
              location: valuesformap[i],
              stopover: true
            });

        }

        directionsService.route({
          origin: company_address,
          destination: '"'+valuesformap[largestindex]+'"',
          waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            // alert('ok');
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var summaryPanel = document.getElementById('directions-panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
              var routeSegment = i + 1;
              summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
                  '</b><br>';
              summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
              summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
              summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
    </script>


   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAl_ZSipciqQS8XLebSVN7FfvOek8tnlt0&libraries=places&callback=initMap"
    async defer></script>
</body>

</html>
