@include('partials.navbar')

<body class="hold-transition skin-purple sidebar-mini" id="fullscreen">
    <!-- Site wrapper -->
    <div class="wrapper">
  @include('partials.asidebar')
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @if (session('message'))
            <div class="w-form-fail" id="message" style="background: green;font-size: 25px;color: white;">
            <div> {{session('message')}} </div>
          </div>  @endif
            <section class="content-header">
                <h1>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Swap Vehicle</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="head">
                            <h1 class="pull-left" >Swap Vehicle</h1>
                            <!-- <button type="button" class="pull-right button raised primary" data-toggle="modal" data-target="#addNew">Add New</button> -->
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-12">
                        <div class="md-button-box">
                            <div class="btn-group">
                                <button class="md-button" data-toggle="modal" data-target="#addNew" onclick="myfunction('addNew')">Add New</button>
                                <button class="md-button" data-toggle="modal" data-target="#edit" onclick="myfunction('edit')">Edit</button>
                                <button class="md-button">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="swap_vehicle" method="POST" id="swap_vehicle">{{csrf_field()}}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="background">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Existing Vehicle Number</label>
                                            <select class="form-control select2" name="existing_vehicle_number" style="width: 100%;">
                                                <option value="" selected="selected">Select Existing Vehicle Number</option>
                                                @foreach($vehicle_set as $set)
                                                <option value='{{$set->vehicle_id}}'>{{$set->vehicle_number}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <label for="existing_vehicle_number" class="error"></label>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label>Replace Vehicle Number</label>
                                            <select class="form-control select2" name="replace_vehicle_number" style="width: 100%;">
                                                <option value="" selected="selected">Select Replace Vehicle Number</option>
                                                @foreach($vehicle_unset as $unset)
                                                <option value='{{$unset->vehicle_id}}'>{{$unset->vehicle_number}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <label for="replace_vehicle_number" class="error"></label>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button class="md-button pull-right">Swap</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </section> <!-- /.content -->
        </div><!-- /.content-wrapper -->

        <!-- Modal Popup for Add New Starts -->
            <div class="modal fade" id="addNew" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Add Office</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="#" method="POST" id="add_office">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label" >Branch Name <span class="red">*</span> </p>
                                        <input type="text" name="branchname" class="form-control" placeholder="Branch Name" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label" >Branch ID <span class="red">*</span></p>
                                        <input type="text" name="branchid" class="form-control" placeholder="Branch ID" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Head Office <span class="red">*</span></p>
                                        <select class="form-control select2" name="headoffice" style="width: 100%;">
                                            <option selected="selected" value="">Select Head Office</option>
                                            <option>Office One</option>
                                            <option>Office Two</option>
                                            <option>Office Three</option>
                                            <option>Office Four</option>
                                            <option>Office Five</option>
                                            <option>Office Six</option>
                                            <option>Office Seven</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Contact Person <span class="red">*</span></p>
                                        <select class="form-control select2" name="contactperson" style="width: 100%;">
                                            <option selected="selected" value="">Select Contact Person</option>
                                            <option>Person One</option>
                                            <option>Person Two</option>
                                            <option>Person Three</option>
                                            <option>Person Four</option>
                                            <option>Person Five</option>
                                            <option>Person Six</option>
                                            <option>Person Seven</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label">Address <span class="red">*</span></p>
                                        <input type="text" name="address" class="form-control" placeholder="Address" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Phone Number <span class="red">*</span></p>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Fax Number</p>
                                        <input type="text" name="fax" class="form-control" placeholder="Fax Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Pincode <span class="red">*</span></p>
                                        <input type="text" name="pincode" class="form-control" placeholder="Pincode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="button raised primary pull-right">Add</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Add New Ends -->

        <!-- Modal Popup for Edit Office Starts -->
            <div class="modal fade" id="edit" role="dialog" aria-labelledby="myModalLabel" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Edit Office</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form action="#" method="POST" id="edit_office">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label" >Branch Name</p>
                                        <input type="text" name="branchname" class="form-control" placeholder="Branch Name" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label" >Branch ID</p>
                                        <input type="text" name="branchid" class="form-control" placeholder="Branch ID" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Head Office</p>
                                        <select id="one" class="form-control select2" name="headoffice" style="width: 100%;">
                                            <option selected="selected" value="">Select Head Office</option>
                                            <option>Office One</option>
                                            <option>Office Two</option>
                                            <option>Office Three</option>
                                            <option>Office Four</option>
                                            <option>Office Five</option>
                                            <option>Office Six</option>
                                            <option>Office Seven</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Contact Person</p>
                                         <select class="form-control select2" name="contactperson" style="width: 100%;">
                                            <option selected="selected" value="">Select Contact Person</option>
                                            <option>Person One</option>
                                            <option>Person Two</option>
                                            <option>Person Three</option>
                                            <option>Person Four</option>
                                            <option>Person Five</option>
                                            <option>Person Six</option>
                                            <option>Person Seven</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="form-label">Address</p>
                                        <input type="text" name="address" class="form-control" placeholder="Address" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Phone Number</p>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Fax Number</p>
                                        <input type="text" name="" class="form-control" placeholder="Fax Number" />
                                    </div>
                                    <div class="form-group">
                                        <p class="form-label">Pincode</p>
                                        <input type="text" name="pincode" class="form-control" placeholder="Pincode" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="button raised primary pull-right">Update</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>

        <!-- Modal Popup for Edit Office Ends -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.3
            </div>
            <strong>Copyright &copy; 2018-2019 <a href="#">Magic Travels</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
   <!-- jQuery 2.2.0 -->
    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/form-validator.js"></script>
    <script type="text/javascript" src="dist/js/custom-validation.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/jquery.fullscreen.min.js"></script>
    <script src="dist/js/default.js"></script>
    <script type="text/javascript">
         $('#swap_vehicle_li').addClass('active');
        function myfunction(x)
        {
            ModalPass(x);
        }



    $(function () {
        $(".select2").select2();
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: true
        });
      });
     $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });

    </script>
</body>

</html>
