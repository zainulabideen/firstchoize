<!DOCTYPE html>
<html>
<head>
<title>Place Autocomplete</title>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<style>

#map {
height: 60%;
width:80%;
margin-left:auto;
margin-right:auto;
border:8px solid #fff;

box-shadow:2px 1px 1px 4px #ccc;
}
/* Optional: Makes the sample page fill the window. */
html, body {
height: 100%;
margin: 0;
padding: 0;
}

#pac-input, #location-address{
background-color: #fff;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 75%;

margin:1% 3% 1% 10%;
}

#title {
color: #fff;
background-color: #4d90fe;
font-size: 25px;
font-weight: 500;
padding: 6px 12px;
}
</style>
</head>
<body>

<input id="pac-input" type="text"
placeholder="Enter a location">


<div id="map"></div>

<div id="location-address"></div>
<input id="address" type="hidden" value="Suite LP34740 20-22 Wenlock Road London N1 7GU">
<script>
var geocoder;
var map;
var marker;
var infowindow = new google.maps.InfoWindow({size: new google.maps.Size(150,50)});
function initMap() {
geocoder = new google.maps.Geocoder();
var latlng = new google.maps.LatLng(51.530633, -0.093570);
var mapOptions = {
zoom:6,
center: latlng,
mapTypeId: google.maps.MapTypeId.ROADMAP
}

map = new google.maps.Map(document.getElementById('map'), mapOptions);

var input = document.getElementById('pac-input');

var autocomplete = new google.maps.places.Autocomplete(input);

autocomplete.bindTo('bounds', map);

var address = document.getElementById('address').value;
geocoder.geocode( { 'address': address}, function(results, status) {
if (status == google.maps.GeocoderStatus.OK) {
map.setCenter(results[0].geometry.location);
if (marker) {
marker.setMap(null);
if (infowindow) infowindow.close();
}
marker = new google.maps.Marker({
map: map,
draggable: true,
position: results[0].geometry.location
});
google.maps.event.addListenerOnce(map, 'idle', function () {

google.maps.event.trigger(map, 'resize');

});
google.maps.event.addListener(marker, 'dragend', function() {

geocodePosition(marker.getPosition());
});

google.maps.event.trigger(marker, 'click');
} else {
alert('Geocode was not successful for the following reason: ' + status);
}
});

autocomplete.addListener('place_changed', function() {

marker.setVisible(false);
var place = autocomplete.getPlace();
if (!place.geometry) {
window.alert("No details available for input: '" + place.name + "'");
return;
}

if (place.geometry.viewport) {
map.fitBounds(place.geometry.viewport);
} else {
map.setCenter(place.geometry.location);
map.setZoom(17); // Why 17? Because it looks good.
}
marker.setPosition(place.geometry.location);
marker.setVisible(true);

var address = '';
if (place.address_components) {
address = [
(place.address_components[0] && place.address_components[0].short_name || ''),
(place.address_components[1] && place.address_components[1].short_name || ''),
(place.address_components[2] && place.address_components[2].short_name || ''),
(place.address_components[3] && place.address_components[3].short_name || ''),
(place.address_components[4] && place.address_components[4].short_name || ''),
(place.address_components[5] && place.address_components[5].short_name || '')
].join(' ');
}

document.getElementById('location-address').innerHTML=place.name+', '+address;

});

}

function geocodePosition(pos) {
geocoder.geocode({
latLng: pos
}, function(responses) {
if (responses && responses.length > 0) {
marker.formatted_address = responses[0].formatted_address;
} else {
marker.formatted_address = 'Cannot determine address at this location.';
}

document.getElementById('location-address').innerHTML=marker.formatted_address;
});
}

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAl_ZSipciqQS8XLebSVN7FfvOek8tnlt0&libraries=places&callback=initMap"
async defer></script>
</body>
</html>
